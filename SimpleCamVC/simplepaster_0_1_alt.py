#SimplePaster 0.1 by Ted Lougheed - simple image pasting utility
#Last updated: 25-Jan-2013
#
#This program is designed for a very specific kind of image pasting.
#Essentially, it just loads up one image file and allows the user to paste
#it into another image file at the desired location.


import pygame, math, sys, csv
import numpy, scipy, cv
sys.path.append("C:\OpenCV2.2\Python2.7\Lib\site-packages")
from pygame.locals import *

#initialize pygame module
pygame.init()

#define main function
def main():
    
    #define cursor position
    cursorPos = 0,0

    #set up colors
    TRANSPARENT = (0,0,1)
    BLACK = (0,0,0)
    WHITE = (255,255,255)
    RED = (255,0,0)
    GREEN = (0,255,0)
    BLUE = (0,0,255)
    DARKBLUE = (0,0,127)
    LIGHTBLUE = (127,127,255)

    #set up window
    #set window caption
    pygame.display.set_caption('SimplePaster 0.1')
    #load and set logo
    #create the main surface on the screen
    screenWidth = 640
    screenHeight = 480
    screen = pygame.display.set_mode((screenWidth, screenHeight), DOUBLEBUF)
    pygame.display.flip()
  
    #set up fonts
    titleFont = pygame.font.SysFont(None, 16)
    basicFont = pygame.font.SysFont(None, 12)

    #set up clock
    clock = pygame.time.Clock()
    FRAMES_PER_SECOND = 30

    #set up text (temp)
    #text = titleFont.render('SimplePaster 0.1', True, WHITE, BLACK)
    #textRect = text.get_rect()
    #textRect.left = 16
    #textRect.top = 16

    #imgSelect = pygame.Surface((180,200))
    #imgSelect.set_colorkey(BLACK)
    #pygame.draw.ellipse(imgSelect, RED, (0,0,180,200), 1)

    #OLD set up world surface
    #worldWidth = 800
    #worldHeight = 600
    #worldSurf = pygame.Surface((worldWidth,worldHeight))
    
    #draw background
    screen.fill(BLACK)
    #OLD worldSurf.fill(BLACK)
    #OLD screen.blit(worldSurf,(((worldWidth/2)-(screenWidth/2)),((worldHeight/2)-(screenHeight/2))))
            
    #define a variable to control the main loop
    running = True

    ##TESTING CAMERA CAPTURE

    #get the picture
    bgFileName = raw_input('Enter filename (JPG file, no extension):')
    bgFileName = bgFileName + '.jpg'
    bgImage = pygame.image.load(bgFileName).convert()

    #get the to-be-pasted image
    fgFileName = 'stickynew.png'
    fgImage = pygame.image.load(fgFileName).convert()
    fgImage.set_colorkey(BLACK)
    
    #Show the background image on the screen

    screen.blit(bgImage, (0,0))

    #flip the display
        
    pygame.display.flip()

    #TEMP load input image
    #inputImgName = raw_input('Enter the name of the input image to load: ')
    #inputImg = pygame.image.load(inputImgName)
    #inputImgRect = inputImg.get_rect()

    #main loop
    while running:
        #advance the clock 1 tick
        clock.tick(FRAMES_PER_SECOND)

        ##TEMP pygame.camera.list_cameras()

        #USER_INPUT
        #event handling, gets all events from the eventqueue
        for event in pygame.event.get():
            #if the user clicks the X, quit
            if event.type == QUIT:
                #change value to false, exit main loop
                running = False
              #  sys.exit()
            if event.type == MOUSEMOTION:
                cursorPos = event.pos
                #display background image
                screen.blit(bgImage, (0,0))
                #display input image on top of background image
                fgImageRect = fgImage.get_rect()
                screen.blit(fgImage, ((cursorPos[0] - fgImageRect.centerx),(cursorPos[1] - fgImageRect.centery)))
                    
            if not hasattr(event, 'key'): continue
            #exit loop if the player presses Esc
            if (event.key == K_ESCAPE):
                running = False
            
            #paste image
            if (event.type == KEYUP):
                if (event.key == K_SPACE):
                                                                 
                    #save the new image
                    outputFileName = 'new.png'
                    pygame.image.save(screen, outputFileName)
                    bgImage = pygame.image.load(outputFileName).convert()
                
        #lastly, flip the display
        pygame.display.flip()

    #If the running flag is false, quit the program.
    if running == False:
        pygame.quit()

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":
    #call main function
    main()

import pygame, math, random, sys, os, csv
from pygame.locals import *

pygame.init()
pygame.mixer.init() #initialize sound

pygame.mouse.set_visible(False)

#GRAPHICS

#define color words
TRANSPARENT = (0,0,1) #set aside almost-black for use as colorkey
BLACK = (0,0,0)
WHITE = (255,255,255)
SILVER = (192,192,192)
GRAY = (128,128,128)
RED = (255,0,0)
MAROON = (128,0,0)
YELLOW = (255,255,0)
OLIVE = (128,128,0)
LIME = (0,255,0)
GREEN = (0,128,0)
AQUA = (0,255,255)
TEAL = (0,128,128)
BLUE = (0,0,255)
NAVY = (0,0,128)
FUCHSIA = (255,0,255)
PURPLE = (128,0,128)
SUNSET = (255,128,128)
ORANGE = (255,128,0)
LIGHTGREEN = (128, 255, 128)
UN_BLUE = (128,128,255)
AZURE = (0,128,255)
NEUTRAL_BLUE = (153,216,223)

#define fonts
basicFont = pygame.font.SysFont(None, 72)
smallFont = pygame.font.SysFont(None, 36)
titleFont = pygame.font.SysFont(None, 72)

#define constants
FPS = 30 #frames per second. Changing this will affect some of the timings. Tested at 60 FPS
MASK_TIMEOUT = FPS/2 #time that the mask is shown, in seconds
WAIT_TIME = 3 * FPS #time between showing the mask and showing the next word
WORD_TIMEOUT = 10 * FPS #time that the target word is displayed
DELTA_X = 1.2 #amount of change in horizontal movement per keystroke

#load sound files
qHowWellYou = pygame.mixer.Sound('howwellyou.wav')
qHowWellSteve = pygame.mixer.Sound('howwellsteve.wav')
qHowWellObama = pygame.mixer.Sound('howwellobama.wav')
qHowWellJustin = pygame.mixer.Sound('howwelljustin.wav')
qHowWellLohan = pygame.mixer.Sound('howwelllohan.wav')

#lists to hold data, keyboard inputs, and used words
kLog = list()
subjLog = list()
#wordLog = list()

#container for the words read from the file
originalWordList = list()

#container for randomly ordered words from originalWordList
randomWordList = list()

#list of indexes of words from original list that have already been
#added to random word list 
usedList1 = list()

#load word set into a list object
#f = csv.reader(open('wordset.txt', 'rb'), delimiter=',', quotechar='|')
#for row in f:
#    for col in row:
#        originalWordList.append(col)

originalWordList = ['Friendly','Sympathetic','Prepared','Mindful','Relaxed','Organized']

#create a new word list with the words from the old list in randomized
#and non-repeating order
        
while len(randomWordList) < 6:
    #DEBUGprint 'Length of random word list: ' + str(len(randomWordList))
    wordFound = False
    randomIndex = random.randint(0,5)
    #DEBUGprint 'Random Index: ' + str(randomIndex)
    #while t == False:
    i = 0
    while i < len(usedList1):
        if usedList1[i] == randomIndex:
            #DEBUGprint 'Dupe: ' + str(usedList1[i]) + ':' + str(i)
            wordFound = True
        i += 1
    if wordFound == False:
        usedList1.append(randomIndex)
        randomWordList.append(originalWordList[randomIndex])

#LOGICAL CLASSES
#a class for the player (not the player's ship)
class player:

    def __init__(self):
        self.score = 0
        self.timesDied = 0

    def die(self):
        self.timesDied += 1

    def increaseScore(self,amount):
        self.score += amount
           
#OBJECT CLASSES

#world class
#NOT USED YET
class world:

    def __init__(self,wSize):
        #PHYSICS
        self.size = wSize #must be a 2-member tuple, e.g. (640,480)
        #GRAPHICS
        self.bgColor = NEUTRAL_BLUE

#object classes
class wall(pygame.sprite.Sprite):

    def __init__(self, posX):
        pygame.sprite.Sprite.__init__(self)
        self.X = posX
        self.Y = 240

        #define surface and image
        self.Img = pygame.Surface((32,128))
        self.Img.fill(BLACK)

        self.rect = self.Img.get_rect()
        
class notch(pygame.sprite.Sprite):

    def __init__(self, posX):
        pygame.sprite.Sprite.__init__(self)
        self.X = posX
        self.Y = 272

        #define surface and image
        self.Img = pygame.Surface((4,64))
        self.Img.fill(GRAY)

        self.rect = self.Img.get_rect()

#player's spacecraft avatar class 
class spacecraft(pygame.sprite.Sprite):

    def __init__(self, posX, posY, pEntity):
        pygame.sprite.Sprite.__init__(self)
        self.X = posX #in world coordinates
        self.Y = posY
        self.playerEntity = pEntity
        self.spawnX = posX
        self.spawnY = posY
        #initial velocity
        self.dX = 0
        while self.dX < 2 and self.dX > -2:
                self.dX = random.uniform(-4,4)
                print self.dX
        
        #modify how much each lateral thrust adds to the velocity
        #again, a number between 0 and 1 is best - increase above 1 at your own risk!
        self.thrustMod = DELTA_X

        self.vulnerable = False #can the ship be damaged? (this will be used for temporary invulnerability at spawn, and from power-ups)
        
        #define surface and image
        self.Img = pygame.Surface((32,32))
        self.Img.set_colorkey(TRANSPARENT)
        self.Img.fill(TRANSPARENT)
        pygame.draw.circle(self.Img, BLACK, (16,16), 16, 0)

        #for collision detection purposes
        self.rect = self.Img.get_rect()
    
    def move(self,key_thrust,wSize):
        if key_thrust == 1:
            #calculate new vector:
            self.dX = self.dX + self.thrustMod
            if self.dX > 0 and self.dX < 1:
                self.dX = 1
            #if self.dX < 0 and self.dX > -1:
            #    self.dX = -1
        elif key_thrust == -1:
            self.dX = self.dX - self.thrustMod
            #if self.dX > 0 and self.dX < 1:
            #    self.dX = 1
            if self.dX < 0 and self.dX > -1:
                self.dX = -1
            
        #move the ship along X axis
        self.X += self.dX
        #wrap ship around world if it crosses the X boundary
        if self.X > (wSize[0] - 40):
            self.X = self.spawnX
            self.dX = 0
            self.playerEntity.die()
            while self.dX < 2 and self.dX > -2:
                self.dX = random.uniform(-4,4)
                print self.dX
        elif self.X < 40:
            self.X = self.spawnX
            self.dX = 0
            self.playerEntity.die()
            while self.dX < 2 and self.dX > -2:
                self.dX = random.uniform(-4,4)
                print self.dX
        
        #update rect for collision detection
        self.rect = self.Img.get_rect()

#GAME
class game:

    def __init__(self,wList):
        #META
        self._running = True
        
        #DISPLAY
        self._surf_display = None
        self.size = self.width, self.height = 1024,600
        self.titleFont = None
        self.basicFont = None
        self.winFont = None

        #define mask image
        self.imgMask = pygame.image.load('whitenoise2.jpg').convert()

        #CONTROL
        self.k_thrust = 0

        #SOUND
        pygame.mixer.init()
        
        #SIMULATION
        #(later these will be put in a class):
        self.wordNumber = 0 #keep track of where we are in the list of words
        self.wordList = wList
        self.wordTimeout = WORD_TIMEOUT
        self.answerFlag = False
        self.rating = 0
        self.maskCounter = 0
        self.waitTime = WAIT_TIME
        
        #|__WORLD
        self.worldSize = self.size
        
        #Set positions of walls and notches
        self.leftWall = wall(0)
        self.rightWall = wall(self.worldSize[0] - 32)
        self.notchL1 = notch((self.worldSize[0]/2) - 34)
        self.notchL2 = notch((self.worldSize[0]/2) - 130)
        self.notchL3 = notch((self.worldSize[0]/2) - 258)
        self.notchR1 = notch((self.worldSize[0]/2) + 30)
        self.notchR2 = notch((self.worldSize[0]/2) + 126)
        self.notchR3 = notch((self.worldSize[0]/2) + 254)
                
        #|__PLAYER
        #create player's "logical" self
        self.player1 = player()
        #create player's ship at the center of the world
        self.playerShip = spacecraft(self.worldSize[0]/2, self.worldSize[1]/2, self.player1)
 
        #GRAPHICS
 
        #set up world surface
        self.worldSurf = pygame.Surface(self.worldSize)
        self.worldSurf.set_colorkey(TRANSPARENT)

    def startUp(self):
        #kickstart the loop
        self._running = True

        #set up fonts
        self.basicFont = pygame.font.SysFont(None, 24)
        self.titleFont = pygame.font.SysFont(None, 48)
        self.winFont = pygame.font.SysFont(None, 72)

        #set up display surface
        self._surf_display = pygame.display.set_mode(self.size, DOUBLEBUF)
        
        #fill display surface with color (probably won't be seen)
        self._surf_display.fill(NEUTRAL_BLUE)
                
    def eventmanager(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        #check for keypresses    
        elif event.type == KEYDOWN:
            #if the player presses Esc, quit
            if event.key == K_ESCAPE:
                self._running = False
            if event.key == K_LEFT:
                self.k_thrust = -1
            if event.key == K_RIGHT:
                self.k_thrust = 1
            if self.answerFlag == False and self.waitTime == 0:
                if event.key == K_1:#descriptiveness rating 1 (lowest)
                    self.rating = 1
                    self.answerFlag = True
                if event.key == K_2:#descriptiveness rating 2
                    self.rating = 2
                    self.answerFlag = True
                if event.key == K_3:#descriptiveness rating 3
                    self.rating = 3
                    self.answerFlag = True
                if event.key == K_4:#descriptiveness rating 4
                    self.rating = 4
                    self.answerFlag = True  
        elif event.type == KEYUP:
            if event.key == K_LEFT:
                self.k_thrust = 0
            if event.key == K_RIGHT:
                self.k_thrust = 0
        
    def mainMenu(self):
        pass

    def simulation(self):

        #---MOVEMENT
        self.playerShip.move(self.k_thrust,self.worldSize)
 
    def on_render(self):
        
        #Draw background
        #i'll have to change this later, since where the worldSurf
        #appears on screen will depend on where the player spawns.
        #Once multiplayer is in place, it will be given by the server
        self.worldSurf.fill(NEUTRAL_BLUE)
        self._surf_display.blit(self.worldSurf,(((self.worldSize[0]/2)-(self.size[0]/2)),((self.worldSize[1]/2)-(self.size[1]/2))))
               
        #Wipe the screen (later I'll just update the dirty_rects)
        self.worldSurf.fill(NEUTRAL_BLUE)
    
        #SHIP
        
        #position ship on screen
        playerShipImg = self.playerShip.Img
        playerRect = playerShipImg.get_rect()
        playerRect.center = (self.playerShip.X,self.playerShip.Y)
        #render ship to world
        self.worldSurf.blit(playerShipImg, playerRect)
    
        #position walls and notches
        leftWallImg = self.leftWall.Img
        rightWallImg = self.rightWall.Img
        leftWallRect = leftWallImg.get_rect()
        rightWallRect = rightWallImg.get_rect()
        leftWallRect.topleft = (self.leftWall.X,self.leftWall.Y)
        rightWallRect.topleft = (self.rightWall.X,self.rightWall.Y)

        notchL1Rect = self.notchL1.Img.get_rect()
        notchL1Rect.topleft = (self.notchL1.X,self.notchL1.Y)
        notchL2Rect = self.notchL2.Img.get_rect()
        notchL2Rect.topleft = (self.notchL2.X,self.notchL2.Y)
        notchL3Rect = self.notchL3.Img.get_rect()
        notchL3Rect.topleft = (self.notchL3.X,self.notchL3.Y)
        notchR1Rect = self.notchR1.Img.get_rect()
        notchR1Rect.topleft = (self.notchR1.X,self.notchR1.Y)
        notchR2Rect = self.notchR2.Img.get_rect()
        notchR2Rect.topleft = (self.notchR2.X,self.notchR2.Y)
        notchR3Rect = self.notchR3.Img.get_rect()
        notchR3Rect.topleft = (self.notchR3.X,self.notchR3.Y)

        #render walls and notches
        self.worldSurf.blit(leftWallImg, leftWallRect)
        self.worldSurf.blit(rightWallImg, rightWallRect)
        self.worldSurf.blit(self.notchL1.Img, notchL1Rect)
        self.worldSurf.blit(self.notchL2.Img, notchL2Rect)
        self.worldSurf.blit(self.notchL3.Img, notchL3Rect)
        self.worldSurf.blit(self.notchR1.Img, notchR1Rect)
        self.worldSurf.blit(self.notchR2.Img, notchR2Rect)
        self.worldSurf.blit(self.notchR3.Img, notchR3Rect)

        if self.wordTimeout == 0:
            self.answerFlag = True
            self.rating = 0

        #if answer flag is false, show current word on screen
        #otherwise show mask
        if self.answerFlag == False and self.waitTime == 0:
            textDisp = basicFont.render(self.wordList[self.wordNumber], True, BLACK, TRANSPARENT)
            textDisp.set_colorkey(TRANSPARENT)
            textRect = textDisp.get_rect()
            textRect.left = (self.worldSize[0]/2) - (textDisp.get_width() / 2)
            textRect.top = 190
            self.worldSurf.blit(textDisp, textRect)
            if self.wordTimeout > 0:
                self.wordTimeout -= 1
        elif self.answerFlag == False and self.waitTime > 0:
            self.waitTime -= 1
        elif self.answerFlag == True:
            self.maskCounter += 1
            self.worldSurf.blit(self.imgMask, (288,120))
            if self.maskCounter == MASK_TIMEOUT:
                #kLog.append(str(self.rating))
                #subjLog.append(str(self.randSubj))
                self.rating = 0
                self.answerFlag = False
                self.maskCounter = 0
                self.waitTime = WAIT_TIME
                self.wordTimeout = WORD_TIMEOUT
                if self.wordNumber < 5:
                    self.randSubj = random.randint(0,2)
                    if self.randSubj == 0:
                        qHowWellJustin.play()
                    if self.randSubj == 1:
                        qHowWellObama.play()
                    if self.randSubj == 2:
                        qHowWellLohan.play()
                if self.wordNumber < 6:
                    self.wordNumber += 1
        if self.wordNumber == 6:
            self._running = False
        
        #render world to screen
        self._surf_display.blit(self.worldSurf, (0,0))#, ((self.size[0]/2),(self.size[1]/2)))
            
        #lastly, flip the display
        pygame.display.flip()
          
    def cleanup(self):
        pygame.quit()

    def on_execute(self, FPS):
        if self.startUp() == False:
            self._running = False
        #set up clock
        clock = pygame.time.Clock()
        FRAMES_PER_SECOND = FPS
        self.worldSurf.fill(NEUTRAL_BLUE)
        self.textTitle = titleFont.render('READY', True, BLACK, TRANSPARENT)
        self.textTitle.set_colorkey(TRANSPARENT)
        self.textRect = self.textTitle.get_rect()
        self.textRect.left = self.size[0]/2 - 100
        self.textRect.top = self.size[1]/2 - 32
        self.worldSurf.blit(self.textTitle, self.textRect)
        self._surf_display.blit(self.worldSurf, (0,0))
        pygame.display.flip()
        pygame.time.wait(3000)
        self.worldSurf.fill(NEUTRAL_BLUE)
        self._surf_display.blit(self.worldSurf, (0,0))
        pygame.display.flip()
        pygame.time.wait(1000)
        
        self.randSubj = random.randint(0,2)
        if self.randSubj == 0:
            qHowWellJustin.play()
        if self.randSubj == 1:
            qHowWellObama.play()
        if self.randSubj == 2:
            qHowWellLohan.play()

#instead of reading directly from the file, this now reads from a randomized
#list generated upon initiation. This way, we can just go down the list and
#ask for responses, then just iterate through both lists when saving responses
#to a file
        
##        while (dispNum < 61):
##            randrow = random.randint(1,60)
##            rownum = 1
##            for row in f1:
##                for col in row:
##                    if (rownum == randrow):
##                        if (usedList1.count(col) == 0):
##                            #DEBUG print(col)
##                            tempCount += 1
##                            print tempCount
##                            showMask()
##                            clearScreen()
##                            showText(col)
##                            randInput = random.randint(0,1)
##                            if randInput == 0:
##                                getInput('',1)#getInput('How well does this word describe you?',50)
##                                wordLog.append(col)
##                                subjLog.append('0')
##                            else:
##                                getInput('',1)#getInput('How well does this word describe Stephen Harper?',50)
##                                wordLog.append(col)
##                                subjLog.append('1')
##                            clearScreen()
##                            #display word
##                            #add word to used list so it does not appear again
##                            #DEBUG print(col)
##                            usedList1.append(col)
##                            dispNum += 1
##                rownum += 1
        
        while (self._running):
            #advance the clock 1 tick
            clock.tick(FRAMES_PER_SECOND)
            #for event in pygame.event.get():
            #    self.eventmanagerMenu(event)
            #self.mainMenu()
            for event in pygame.event.get():
                self.eventmanager(event)
            self.simulation()
            self.on_render()
        self.cleanup()

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":

    #create instance of game
    sra = game(randomWordList)
    #execute the game loop at pre-defined FPS
    sra.on_execute(FPS)

##DEBUG ONLY:
##    w = 0
##    while w < 60:
##        #print usedList1[w]
##        print originalWordList[w]
##        w += 1
##
##    w2 = 0
##    while w2 < 60:
##        #print usedList1[w]
##        print randomWordList[w2]
##        w2 += 1
##

##OLD CODE - doesn't work with this version of the program - here for reference only
##    #copy the lists of user feedback info into a CSV file.
##
##    dataFilename = open('sraDataLog.csv', 'wb')
##    dataFile = csv.writer(dataFilename, delimiter=',', quotechar='|')
##
##    i = 1
##    while (len(wordLog) > 0):
##        dataFile.writerow([subjLog.pop(),wordLog.pop(),kLog.pop()])
##        i += 1
##
##    dataFilename.close()

##    dataFilename = open('sraDataLog.csv', 'wb')
##    dataFile = csv.writer(dataFilename, delimiter=',', quotechar='|')
##
##    i = 0
##    while i < 60:
##        dataFile.writerow([subjLog[i],wordLog[i],kLog[i]])
##        i += 1
##
##    dataFilename.close()

#very short program for calculating the dice coefficient for a body of text

def dice(a, b):
        """
        dice coefficient = bigram overlap * 2 / bigrams in a + bigrams in b
        """
        a_bigrams = set(a[i:i+2] for i in range(len(a) - 1))
        b_bigrams = set(b[i:i+2] for i in range(len(b) - 1))
 
        overlap = len(a_bigrams & b_bigrams)
 
        total = len(a_bigrams) + len(b_bigrams)
        dice_coefficient  = overlap * 2.0 / total
 
        return dice_coefficient

def asd():
    astring = str(input('First string:'))
    bstring = str(input('Second string:'))
    
    diceco = dice(astring,bstring)

    print('Dice coefficient: ')
    print(diceco)
    
asd()

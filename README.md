# README #

This repo is a collection of small programs I used during the research portion of my Cognitive Science doctoral project.

The following is an outline of the contents of the various folders:

###dice_co

This calculates the dice coefficient of a body of text. It was intended for use in a data-mining algorithm I developed during one of my methodology rotations. It can be used to quantitatively determine the similarity between two texts.

###Facegrabber

This is a program for a task I didn't end up using in my final studies. It is designed to capture an image of a participant's face and paste it on top of another image. The idea was to get the participant to identify with an actor in a series of scenarios.
There are actually two programs in this folder - facegrabber.py does the image capture and facepaste.py pastes the face onto a series of images.

###Gorb

This is a simple utility for generating sequences of digits. It was used in a pilot study to generate stimuli for a backwards-counting task. The name is a nonsense word - there is no meaning behind it.

###rk analysis

This program was written to automate a tedious data processing task for a version of Endel Tulving's Remember/Know task. The task generated large amounts of data so I wrote this program to generate descriptive statistics about the results. The output file can be imported directly into Excel, SPSS/PASW, or any stats program that supports .CSV files.

###SimpleCamVC

As the name suggests, this is a simple image capture program. One of my tasks required capturing a large sequence of still images. This folder also contains simplepaster.py, which the experimenter could use to quickly paste an image over top of another one without having to mess about with generic image editing software.

###Stimuli Presentation

This folder contains a number of programs used to present stimuli to participants.

####bcts

This reads aloud sequences of numbers that the participant is asked to remember. This task didn't end up getting used and still contains a lot of test code.

####free recall form

This program presented a random series of 16 questions, presented one at a time, about a narrative the participant had heard five minutes earlier. The participant input her answers using the keyboard and the answers were recorded to a CSV file, keyed to the question number.

####inattentional blindness

This program was designed to present a standard inattentional blindness task. In each trial, a number of white 'L' and 'T' shapes bounce around the screen. In the last three of five trials, a '+' shape moves across the screen and the participant is asked afterwards if she saw anything unusual. The program records the answers in a CSV file that can then be input into a statistics package.

####remember_know

This program is an implementation of Endel Tulving's "remember/know" measure of episodic memory recall. A long series of words are presented and participants are asked to classify them as either living or non-living. They are then presented with a subset of those words, mixed in with some novel words, and asked to rate how confident they were that the words either were or were not in the set presented earlier. The results are recorded and a separate program (described above) is used to render those results in a spreadsheet-friendly manner.

####sra

This program is an implementation of a self-referential adjectives task. A series of adjectives are presented, one at a time, and the participant is randomly asked either how well the word describes themselves, or someone else. The questions are presented aurally to prevent interference with the visual stimuli of the adjectives. The results are saved in a CSV file.

####ttcs

This program presents the participant with a simplified version of Tetris, modified to stop after a certain length of time. At the same time, a story is presented aurally - the Tetris game is meant to induce a state of flow in the participant, to see if the flow experience interferes with the participant's encoding of the story in memory.


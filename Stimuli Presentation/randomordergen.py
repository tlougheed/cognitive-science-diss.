#This program generates the numbers 1 through 5 in random order, e.g., 24315, 51342, etc.
#This was used to randomize the order of tasks presented in my Ph.D research.

import random
import pickle

def randomorder():
    print('This program generates the numbers 1 through 5 in random order, e.g., 24315, 51342, etc.')
    
    firstValue = random.randint(1,5)
    secondValue = random.randint(1,5)
    while (secondValue == firstValue): 
        secondValue = random.randint(1,5)
    thirdValue = random.randint(1,5)    
    while (thirdValue == firstValue) or (thirdValue == secondValue): 
        thirdValue = random.randint(1,5)
    fourthValue = random.randint(1,5)
    while (fourthValue == firstValue) or (fourthValue == secondValue) or (fourthValue == thirdValue):
        fourthValue = random.randint(1,5)
    fifthValue = random.randint(1,5)
    while (fifthValue == firstValue) or (fifthValue == secondValue) or (fifthValue == thirdValue) or (fifthValue == fourthValue):
        fifthValue = random.randint(1,5)
        
    print(firstValue)
    print(secondValue)
    print(thirdValue)
    print(fourthValue)
    print(fifthValue)

randomorder()



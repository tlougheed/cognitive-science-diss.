##STIMULI:
##
## 4 sets of 20 2-syllable English nouns - mix of living and non-living
##
##PROCEDURE:
##
##Display random word from first set of 20 2-syllable nouns
##when player presses blue or yellow, end trial
##repeat until list is exhausted
##Do this for second set of 20
##repeat display of both sets 3 more times, randomizing each time (total of 160 trials)
##
##Display blank for 30 s
##
##Display random word from combined set of 40 familiar nouns (randomized)
##when player presses green or red, end trial (yes/no recognition)
##
##Display random word from combined set of 40 familiar nouns (randomized)
##when player presses blue or yellow, end trial (living/nonliving)
##
##Display blank for 120 s
##
##Display 40 randomly-ordered words (20 old, 20 new), one at a time
##when player presses green or red, end trial (yes/no recognition)
##
##Display blank for 120 s
##
##Display all 80 words in pool (4 sets), one at a time (randomized)
##did the word appear in critical study list? (y/n)
##how confident are you about your answer?
##(3 = very confident, 2 = fairly confident, 1 = not at all confident)

#TO-DO:
#
#       Add instructions, where appropriate, and indicate
#            when the critical study list is to be displayed


import pygame, math, sys, csv, random
from pygame.locals import *

pygame.init()

pygame.display.set_caption('_')

pygame.mouse.set_visible(False)

TRANSPARENT = (0,0,1)
BLACK = (0,0,0)
WHITE = (255,255,255)
RED = (255,0,0)
GREEN = (0,128,0)
BLUE = (0,0,255)
BROWN = (150,75,0)
NEUTRAL_BLUE = (153,216,223)

_surf_display = None
_surf_display = pygame.display.set_mode((800,600), FULLSCREEN)
_surf_display.fill(NEUTRAL_BLUE)

background = pygame.Surface((800,600))
background.set_colorkey(TRANSPARENT)
background.fill(NEUTRAL_BLUE)

basicFont = pygame.font.SysFont(None, 72)
smallFont = pygame.font.SysFont(None, 36)

##self.textTitle = self.titleFont.render('PYLOT', True, WHITE, TRANSPARENT)
##self.textTitle.set_colorkey(TRANSPARENT)
##self.textRect = self.textTitle.get_rect()
##self.textRect.left = self.size[0]/2 - 100
##self.textRect.top = self.size[1]/2 - 200
##self._surf_display.blit(self.textTitle, self.textRect)

#list to hold data (keyboard inputs)
kLog = list()
wordLog = list()

f = csv.reader(open('wordset1.txt', 'rb'), delimiter=',', quotechar='|')

usedList1 = list()
newList1 = list()

usedList2 = list()
newList2 = list()

usedListC1 = list()
newListC1 = list()

usedListC2 = list()
newListC2 = list()

usedListTemp = list()
newListTemp = list()

usedListALL = list()
newListALL = list()

#usedList4 = list()
#newList4 = list()

##f1 = csv.reader(open('wordset1.txt', 'rb'), delimiter=',', quotechar='|')    
##for row1 in f1:
##    for col1 in row1:
##        newList1.append(col1)

def showMask():
    imgMask = pygame.image.load('whitenoise.jpg').convert()
    _surf_display.blit(imgMask, (80,60))
    pygame.display.flip()
    pygame.time.wait(500)

def showText(textInput):

    textDisp = basicFont.render(textInput, True, BLACK, TRANSPARENT)
    textDisp.set_colorkey(TRANSPARENT)
    textRect = textDisp.get_rect()
    textRect.left = 400 - (textDisp.get_width() / 2)
    textRect.top = 250
    _surf_display.blit(textDisp, textRect)
    pygame.display.flip()

def getInput(prompt, maxLen, TrialType):#maxLen is the maximum length of a single line of text before a carriage return
    #print 'TrialType (in getInput):' #DEBUG
    #print TrialType #DEBUG
    if not (prompt == None):
        if (len(prompt) < maxLen):
            textDisp = smallFont.render(prompt, True, BLACK, TRANSPARENT)
            textDisp.set_colorkey(TRANSPARENT)
            textRect = textDisp.get_rect()
            textRect.left = 400 - (textDisp.get_width() / 2)
            textRect.top = 350

            _surf_display.blit(textDisp, textRect)

        else:
            tempText1 = prompt[:(maxLen + 1)]
            tempText2 = prompt[(maxLen + 1):]

            textDisp1 = smallFont.render(tempText1, True, BLACK, TRANSPARENT)
            textDisp1.set_colorkey(TRANSPARENT)
            textRect1 = textDisp1.get_rect()
            textRect1.left = 400 - (textDisp1.get_width() / 2)
            textRect1.top = 350

            textDisp2 = smallFont.render(tempText2, True, BLACK, TRANSPARENT)
            textDisp2.set_colorkey(TRANSPARENT)
            textRect2 = textDisp2.get_rect()
            textRect2.left = 400 - (textDisp2.get_width() / 2)
            textRect2.top = 390
            
            _surf_display.blit(textDisp1, textRect1)
            _surf_display.blit(textDisp2, textRect2)
            
        pygame.display.flip()
    else:
        pass
    
    waitForEvent = 1
    while (waitForEvent == 1):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if TrialType == 0:
                    if event.key == K_a:#living (blue)
                        kLog.append('blue')
                        waitForEvent = 0
                    if event.key == K_l:#non-living (yellow)
                        kLog.append('yellow')
                        waitForEvent = 0
                if TrialType == 1:
                    if (event.key == K_x):#yes (green)
                        kLog.append('green')
                        waitForEvent = 0
                        #print TrialType #DEBUG
                    if event.key == K_m:#no (red)
                        kLog.append('red')
                        waitForEvent = 0
                    if event.key == K_y:#yes
                        kLog.append('yes')
                        waitForEvent = 0
                    if event.key == K_n:#no
                        kLog.append('no')
                        waitForEvent = 0
                if TrialType == 2:
                    if event.key == K_1 or event.key == K_KP1:#confidence rating 1
                        kLog.append('c1')
                        waitForEvent = 0
                    if event.key == K_2 or event.key == K_KP2:#confidence rating 2
                        kLog.append('c2')
                        waitForEvent = 0
                    if event.key == K_3 or event.key == K_KP3:#confidence rating 3
                        kLog.append('c3')
                        waitForEvent = 0

def clearScreen():
    _surf_display.blit(background, (0,0))
    pygame.display.flip()

def runTrial1():
    dispNum = 1
    while (dispNum < 21):
        f1 = csv.reader(open('wordset1.txt', 'rb'), delimiter=',', quotechar='|')    
        randrow = random.randint(1,20)
        #DEBUG print(randrow)
        rownum = 1
        for row in f1:
            for col in row:
                if (rownum == randrow):
                    if (usedList1.count(col) == 0):
                        #DEBUG print(col)
                        showText(col)
                        getInput('Living (BLUE) or non-living (YELLOW)?',50,0)
                        wordLog.append(col)
                        clearScreen()
                        #display word
                        #add word to used list so it does not appear again
                        #DEBUG print(col)
                        usedList1.append(col)
                        dispNum += 1
            rownum += 1

    #empty the used list so that we can run though the set of words again
    while len(usedList1) > 0 : usedList1.pop()

    #print(usedList1)
    #print(newList1)

    ##f2 = csv.reader(open('wordset2.txt', 'rb'), delimiter=',', quotechar='|')    
    ##for row1 in f2:
    ##    for col1 in row1:
    ##        newList2.append(col1)

    dispNum = 1
    while (dispNum < 21):
        f2 = csv.reader(open('wordset2.txt', 'rb'), delimiter=',', quotechar='|')    
        randrow = random.randint(1,20)
        rownum = 1
        for row in f2:
            for col in row:
                if (rownum == randrow):
                    if (usedList2.count(col) == 0):
                        showText(col)
                        getInput('Living (BLUE) or non-living (YELLOW)?',50,0)
                        wordLog.append(col)
                        clearScreen()
                        #display word
                        #add word to used list so it does not appear again
                        usedList2.append(col)
                        dispNum += 1
            rownum += 1

    #empty the used list so that we can run though the set of words again
    while len(usedList2) > 0 : usedList2.pop()

    #print(usedList2)
    #print(newList2)

def runTrial2():
    dispNum = 1
    while (dispNum < 41):
        combo1 = csv.reader(open('wordsetcombo12.txt', 'rb'), delimiter=',', quotechar='|')
        randrow = random.randint(1,40)
        rownum = 1
        for row in combo1:
            for col in row:
                if (rownum == randrow):
                    if (usedListC1.count(col) == 0):
                        #display word
                        showText(col)
                        getInput('Was this word in the previous set? Yes (GREEN) or no (RED)?',33,1)
                        wordLog.append(col)
                        clearScreen()
                        #add word to used list so it does not appear again
                        usedListC1.append(col)
                        dispNum += 1
            rownum += 1

    #print('usedListC1: ')
    #print(usedListC1)

    while len(usedListC1) > 0 : usedListC1.pop()

def runTrial3():
    dispNum = 1
    while (dispNum < 41):
        combo1 = csv.reader(open('wordsetcombo12.txt', 'rb'), delimiter=',', quotechar='|')
        randrow = random.randint(1,40)
        rownum = 1
        for row in combo1:
            for col in row:
                if (rownum == randrow):
                    if (usedListC1.count(col) == 0):
                        #display word
                        showText(col)
                        getInput('Living (BLUE) or nonliving (YELLOW)?',50,0)
                        wordLog.append(col)
                        clearScreen()
                        #add word to used list so it does not appear again
                        usedListC1.append(col)
                        dispNum += 1
            rownum += 1

    while len(usedListC1) > 0 : usedListC1.pop()

def runTrial4():
    ##Critical study:
    ##Display 40 randomly-ordered words (20 old, 20 new), one at a time
    ##when player presses green or red, end trial (yes/no recognition)

    #grab 20 random words from combined word list, add them to new list

    dispNum = 1
    while (dispNum < 21):
        combo1 = csv.reader(open('wordsetcombo12.txt', 'rb'), delimiter=',', quotechar='|')
        randrow = random.randint(1,40)
        rownum = 1
        for row in combo1:
            for col in row:
                if (rownum == randrow):
                    if (usedListTemp.count(col) == 0):
                        #add word to new list
                        newListTemp.append(col)
                        #add word to used list so it does not appear again
                        usedListTemp.append(col)
                        dispNum += 1
            rownum += 1

    #grab all words from 3rd set in random order, add them to new list

    dispNum = 1
    while (dispNum < 21):
        combo1 = csv.reader(open('wordset3.txt', 'rb'), delimiter=',', quotechar='|')
        randrow = random.randint(1,20)
        rownum = 1
        for row in combo1:
            for col in row:
                if (rownum == randrow):
                    if (usedListTemp.count(col) == 0):
                        #add word to new list
                        newListTemp.append(col)
                        #add word to used list so it does not appear again
                        usedListTemp.append(col)
                        dispNum += 1
            rownum += 1

    #randomize the new list

    i = 1
    maxIndex = 39
    randIndex = random.randint(0,maxIndex)
    while (i < 41):
        randIndex = random.randint(0,maxIndex)
        if (maxIndex <= 0):
            randIndex = 1
        newListC2.append(newListTemp.pop(randIndex-1))
        maxIndex = maxIndex - 1
        i += 1

    i = 1
    while (len(newListC2) > 0):
        #display words from randomized list
        uText = newListC2.pop()
        showText(uText)
        getInput('Was this word in the previous set? Yes (GREEN) or no (RED)?',33,1)
        wordLog.append(uText)
        clearScreen()
                        
def runTrial5():
    ##Display all 80 words in pool (all 4 sets), one at a time (randomized)
    ##did the word appear in critical study list? (y/n)
    ##how confident are you about your answer? (3 = very confident, 2 = fairly confident, 1 = not at all confident)
    
    dispNum = 1
    while (dispNum < 81):
        allsets = csv.reader(open('wordlist.txt', 'rb'), delimiter=',', quotechar='|')
        randrow = random.randint(1,80)
        rownum = 1
        for row in allsets:
            for col in row:
                if (rownum == randrow):
                    if (usedListALL.count(col) == 0):
                        #display word
                        showText(col)
                        getInput('Was this word in the critical study list? Yes (GREEN) or no (RED)?',40,1)
                        clearScreen()
                        showText(col)
                        getInput('How confident are you in your response? (1) Not at all confident, (2) Fairly confident (3) Very confident?',39,2)
                        wordLog.append(col)
                        clearScreen()
                        #add word to used list so it does not appear again
                        usedListALL.append(col)
                        dispNum += 1
            rownum += 1


##Display random word from first set of 20 2-syllable nouns
##when player presses blue or yellow, end trial
##repeat until list is exhausted
##Do this for second set of 20

runTrial1()

##repeat display of both sets 3 more times, randomizing each time (total of 160 trials)

runTrial1()
runTrial1()
runTrial1()

##Display blank for 30 s

showMask() #this will display a random pattern of white and black pixels to remove after-image
clearScreen()
showText('BREAK (30 sec)')
pygame.time.wait(30000)
showMask()
clearScreen()
pygame.event.clear()

##Display random word from combined set of 40 familiar nouns (randomized)
##when player presses green or red, end trial (yes/no recognition)

runTrial2()

##Display random word from combined set of 40 familiar nouns (randomized)
##when player presses blue or yellow, end trial (living/nonliving)

runTrial3()

##Display blank for 120 s

showMask()
clearScreen()
showText('BREAK (2 mins)')
#DEBUG pygame.time.wait(3000)
pygame.time.wait(120000)
showMask()
clearScreen()
pygame.event.clear()

##Critical study:
##Display 40 randomly-ordered words (20 old, 20 new), one at a time
##when player presses green or red, end trial (yes/no recognition)

#showText('Critical Study List')

runTrial4()

##Display blank for 120 s

showMask()
clearScreen()
showText('BREAK (2 mins)')
pygame.time.wait(120000)
showMask()
clearScreen()
pygame.event.clear()

##Display all 80 words in pool (4 sets), one at a time (randomized)
##did the word appear in critical study list? (y/n)
##how confident are you about your answer? (3 = very confident, 2 = fairly confident, 1 = not at all confident)

runTrial5()

showText('Finished')

#reverse the key log and word log lists so that lists will be in the same order as presented
kLog.reverse()
wordLog.reverse()

#copy the lists of user feedback info into a CSV file.

dataFilename = open('dataLog.csv', 'wb')
dataFile = csv.writer(dataFilename, delimiter=',', quotechar='|')

i = 1
while (len(wordLog) > 0):
    keyLog = kLog.pop()
    if ((keyLog == 'green') or (keyLog == 'red') or (keyLog == 'blue') or (keyLog == 'yellow') or (keyLog == 'yes') or (keyLog == 'no')) and (i > 280):#there are 280 trials before Trial Set 5 - change this number if you have problems with the data file
        dataFile.writerow([wordLog.pop(),keyLog,kLog.pop()])
    else:
        dataFile.writerow([wordLog.pop(),keyLog])
    i += 1

dataFilename.close()

pygame.quit()


##GARBAGE
#this next step isn't necessary since we empty the used lists after use
##usedList1a = usedList1
##usedList2a = usedList2
##
##i = 1
##while (i < 21):
##    poppedNoun = usedList1a.pop()
##    poppedNoun2 = usedList2a.pop()
##    usedList12.append(poppedNoun)
##    usedList12.append(poppedNoun2)
##    i += 1
##
##print(usedList12)
##w1File = open('wordlistcombo12.txt', 'wb')
##w1 = csv.writer(w1File, delimiter=',', quotechar='|')
##
##i = 1
##while (i < 41):
##    poppedNoun12 = usedList12.pop()
##    w1.writerow([str(poppedNoun12)])
##    i += 1
##
##w1File.close()

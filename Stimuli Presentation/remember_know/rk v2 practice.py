##This is a simplified version of the Remember/Know task,
##intended as a practice run for participants

import pygame, math, sys, csv, random
from pygame.locals import *

pygame.init()

pygame.display.set_caption('_')

pygame.mouse.set_visible(False)

TRANSPARENT = (0,0,1)
BLACK = (0,0,0)
WHITE = (255,255,255)
RED = (255,0,0)
GREEN = (0,128,0)
BLUE = (0,0,255)
BROWN = (150,75,0)
NEUTRAL_BLUE = (153,216,223)

_surf_display = None
_surf_display = pygame.display.set_mode((800,600), FULLSCREEN)
_surf_display.fill(NEUTRAL_BLUE)

background = pygame.Surface((800,600))
background.set_colorkey(TRANSPARENT)
background.fill(NEUTRAL_BLUE)

basicFont = pygame.font.SysFont(None, 72)
smallFont = pygame.font.SysFont(None, 36)

##self.textTitle = self.titleFont.render('PYLOT', True, WHITE, TRANSPARENT)
##self.textTitle.set_colorkey(TRANSPARENT)
##self.textRect = self.textTitle.get_rect()
##self.textRect.left = self.size[0]/2 - 100
##self.textRect.top = self.size[1]/2 - 200
##self._surf_display.blit(self.textTitle, self.textRect)

#list to hold data (keyboard inputs)
kLog = list()
wordLog = list()

f = csv.reader(open('wordset1.txt', 'rb'), delimiter=',', quotechar='|')

usedList1 = list()
newList1 = list()

usedList2 = list()
newList2 = list()

usedListC1 = list()
newListC1 = list()

usedListC2 = list()
newListC2 = list()

usedListTemp = list()
newListTemp = list()

usedListALL = list()
newListALL = list()

#usedList4 = list()
#newList4 = list()

##f1 = csv.reader(open('wordset1.txt', 'rb'), delimiter=',', quotechar='|')    
##for row1 in f1:
##    for col1 in row1:
##        newList1.append(col1)

def showMask():
    imgMask = pygame.image.load('whitenoise.jpg').convert()
    _surf_display.blit(imgMask, (80,60))
    pygame.display.flip()
    pygame.time.wait(500)

def showText(textInput):

    textDisp = basicFont.render(textInput, True, BLACK, TRANSPARENT)
    textDisp.set_colorkey(TRANSPARENT)
    textRect = textDisp.get_rect()
    textRect.left = 400 - (textDisp.get_width() / 2)
    textRect.top = 250
    _surf_display.blit(textDisp, textRect)
    pygame.display.flip()

def getInput(prompt, maxLen, TrialType):#maxLen is the maximum length of a single line of text before a carriage return
    #print 'TrialType (in getInput):' #DEBUG
    #print TrialType #DEBUG
    if not (prompt == None):
        if (len(prompt) < maxLen):
            textDisp = smallFont.render(prompt, True, BLACK, TRANSPARENT)
            textDisp.set_colorkey(TRANSPARENT)
            textRect = textDisp.get_rect()
            textRect.left = 400 - (textDisp.get_width() / 2)
            textRect.top = 350

            _surf_display.blit(textDisp, textRect)

        else:
            tempText1 = prompt[:(maxLen + 1)]
            tempText2 = prompt[(maxLen + 1):]

            textDisp1 = smallFont.render(tempText1, True, BLACK, TRANSPARENT)
            textDisp1.set_colorkey(TRANSPARENT)
            textRect1 = textDisp1.get_rect()
            textRect1.left = 400 - (textDisp1.get_width() / 2)
            textRect1.top = 350

            textDisp2 = smallFont.render(tempText2, True, BLACK, TRANSPARENT)
            textDisp2.set_colorkey(TRANSPARENT)
            textRect2 = textDisp2.get_rect()
            textRect2.left = 400 - (textDisp2.get_width() / 2)
            textRect2.top = 390
            
            _surf_display.blit(textDisp1, textRect1)
            _surf_display.blit(textDisp2, textRect2)
            
        pygame.display.flip()
    else:
        pass
    
    waitForEvent = 1
    while (waitForEvent == 1):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if TrialType == 0:
                    if event.key == K_a:#living (blue)
                        kLog.append('blue')
                        waitForEvent = 0
                    if event.key == K_l:#non-living (yellow)
                        kLog.append('yellow')
                        waitForEvent = 0
                if TrialType == 1:
                    if (event.key == K_x):#yes (green)
                        kLog.append('green')
                        waitForEvent = 0
                        #print TrialType #DEBUG
                    if event.key == K_m:#no (red)
                        kLog.append('red')
                        waitForEvent = 0
                    if event.key == K_y:#yes
                        kLog.append('yes')
                        waitForEvent = 0
                    if event.key == K_n:#no
                        kLog.append('no')
                        waitForEvent = 0
                if TrialType == 2:
                    if event.key == K_1:#confidence rating 1
                        kLog.append('c1')
                        waitForEvent = 0
                    if event.key == K_2:#confidence rating 2
                        kLog.append('c2')
                        waitForEvent = 0
                    if event.key == K_3:#confidence rating 3
                        kLog.append('c3')
                        waitForEvent = 0

def clearScreen():
    _surf_display.blit(background, (0,0))
    pygame.display.flip()

def runTrial1():
    dispNum = 1
    while (dispNum < 21):
        f1 = csv.reader(open('wordset1.txt', 'rb'), delimiter=',', quotechar='|')    
        randrow = random.randint(1,20)
        #DEBUG print(randrow)
        rownum = 1
        for row in f1:
            for col in row:
                if (rownum == randrow):
                    if (usedList1.count(col) == 0):
                        #DEBUG print(col)
                        showText(col)
                        getInput('Living (BLUE) or non-living (YELLOW)?',50,0)
                        wordLog.append(col)
                        clearScreen()
                        #display word
                        #add word to used list so it does not appear again
                        #DEBUG print(col)
                        usedList1.append(col)
                        dispNum += 1
            rownum += 1

    #empty the used list so that we can run though the set of words again
    while len(usedList1) > 0 : usedList1.pop()

    #print(usedList1)
    #print(newList1)

    ##f2 = csv.reader(open('wordset2.txt', 'rb'), delimiter=',', quotechar='|')    
    ##for row1 in f2:
    ##    for col1 in row1:
    ##        newList2.append(col1)

    dispNum = 1
    while (dispNum < 21):
        f2 = csv.reader(open('wordset2.txt', 'rb'), delimiter=',', quotechar='|')    
        randrow = random.randint(1,20)
        rownum = 1
        for row in f2:
            for col in row:
                if (rownum == randrow):
                    if (usedList2.count(col) == 0):
                        showText(col)
                        getInput('Living (BLUE) or non-living (YELLOW)?',50,0)
                        wordLog.append(col)
                        clearScreen()
                        #display word
                        #add word to used list so it does not appear again
                        usedList2.append(col)
                        dispNum += 1
            rownum += 1

    #empty the used list so that we can run though the set of words again
    while len(usedList2) > 0 : usedList2.pop()

    #print(usedList2)
    #print(newList2)

##Display random word from first set of 20 2-syllable nouns
##when player presses blue or yellow, end trial

runTrial1()

showText('Finished')

pygame.quit()

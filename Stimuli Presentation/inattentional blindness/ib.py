##PROCEDURE:
##
##Trial 0 (Practice trial):
##Same as Trial 1
##
##Trial 1 (15000 ms long):
##display fixation point
##display blue centerline
##display L's and T's
##move L's and T's in random directions, bouncing off the sides of the screen
##end of trial: prompt for number of times the black shapes crossed center line
##
##Trial 2:
##same as 1
##
##Trial 3:
##same as 1, but at 5 seconds, move gray cross horizontally across the screen at fixed speed from right to left
##end of trial: prompt for number of times the L's crossed centerline, and prompt
##for whether or not they saw anything unusual (score +1 if yes)
##if yes, prompt for what they saw (score +1 if correctly identified)
##
##Trial 4&5 same as 3

##TO-DO:
## * indicates essential feature
##
##      fix bugs in L and T bouncing behaviour (I haven't really noticed any in a while)

import pygame, math, random, sys, os, csv
from pygame.locals import *
os.environ['SDL_VIDEO_CENTERED'] = '1'

#PHYSICS

#math functions that we'll be using
def atan2(rise, run):
    return math.degrees(math.atan2(rise, run))

#def angleNorm(angle):
    #returns an angle between -180 and 180
    #return (angle + 180) % 360 - 180

def angleNorm(angle):
    #returns an angle between -180 and 180
    return (angle) % 360

#initiate pygame
pygame.init()
#pygame.mixer.init()
        
#GRAPHICS

#define color words
TRANSPARENT = (0,0,1) #set aside almost-black for use as colorkey
BLACK = (0,0,0)
WHITE = (255,255,255)
SILVER = (164,164,164)
GRAY = (128,128,128)
RED = (255,0,0)
MAROON = (128,0,0)
YELLOW = (255,255,0)
OLIVE = (128,128,0)
LIME = (0,255,0)
GREEN = (0,128,0)
AQUA = (0,255,255)
TEAL = (0,128,128)
BLUE = (0,0,255)
NAVY = (0,0,128)
FUCHSIA = (255,0,255)
PURPLE = (128,0,128)
SUNSET = (255,128,128)
ORANGE = (255,128,0)
LIGHTGREEN = (128, 255, 128)
UN_BLUE = (128,128,255)
AZURE = (0,128,255)

dataFilename = open('IBdataLog.csv', 'a')
dataFile = csv.writer(dataFilename, delimiter=',', quotechar='|')

dataFile.writerow(['New participant'])

inputBuffer = str()

class userResponse:
    def __init__(self):
        self.UR = 0

    def getUR(self):
        return self.UR

    def incUR(self):
        self.UR = self.UR + 1

    def setUR(self, response):
        self.UR = response

#instantiate user response object
uR = userResponse()

class letter:

    def __init__(self,imgSurf):
        #define position of letter
        #self.X = XYpos[0]
        #self.Y = XYpos[1]

        self.letterSurf = imgSurf
        self.letterSurf.set_colorkey(TRANSPARENT)
        self.letterRect = self.letterSurf.get_rect()

        self.letterRect.left = (random.random() * 692) + 36
        self.letterRect.top = (random.random() * 424) + 52
        
        self.dX = random.uniform(-1,1) * 6 #max speed of 6 pixels per frame, which at 30 fps is just under 5 cm/s
        self.dY = random.uniform(-1,1) * 6

        while (self.dX < 3.0) and (self.dX > -3.0): #if the speed is less than 3 pixels per frame, try again
            self.dX = random.uniform(-1,1) * 6
        if (self.dX > 4.0):
            while (self.dY < 3.0) and (self.dY > -3.0):
                self.dY = random.uniform(-1,1) * 5
        else:
            while (self.dY < 3.0) and (self.dY > -3.0):
                self.dY = random.uniform(-1,1) * 6 
            
    def draw(self, renderSurface):
        renderSurface.blit(self.letterSurf,(self.letterRect.left,self.letterRect.top))

    def update(self):
##        while (self.dX < 0.5) and (self.dX > -0.5):
##            self.dX = random.uniform(-1,1) * 3
##        while (self.dY < 0.5) and (self.dY > -0.5):
##            self.dY = random.uniform(-1,1) * 3 

        if (self.letterRect.left < 728) and (self.letterRect.left > 0):
            self.letterRect.left += self.dX
        elif (self.letterRect.left <= 0):
            self.letterRect.left = 1
            self.dX = -(self.dX)
            self.letterRect.left += self.dX
        else:
            self.dX = -(self.dX)
            self.letterRect.left += self.dX
        if (self.letterRect.top < 476) and (self.letterRect.top > 52):
            self.letterRect.top += self.dY
        elif (self.letterRect.top <= 52):
            self.letterRect.top = 53
            self.dY = -(self.dY)
            self.letterRect.top += self.dY
        else:
            self.dY = -(self.dY)
            self.letterRect.top += self.dY

class Cross:

    def __init__(self,imgSurf):
        
        self.crossSurf = imgSurf
        self.crossSurf.set_colorkey(TRANSPARENT)
        self.crossRect = self.crossSurf.get_rect()

        self.crossRect.left = 800 + self.crossSurf.get_width()
        #change the following value depending on where you want it relative to the center line
        #currently it is set to move along the center line. This is where most people can see it.
        #According to Most et al. (2000), moving it farther from the center line decreases the likelihood
        #of people detecting it. I should leave it moving along the center line unless I get a ceiling effect in pilot
        self.crossRect.top = 300 - (self.crossSurf.get_height()/2)                
        
        self.dX = 5
        #DON'T NEED THIS ANYMORE self.dY = 3
        
    def draw(self, renderSurface):
        renderSurface.blit(self.crossSurf,(self.crossRect.left,self.crossRect.top))

    def update(self):
        if self.crossRect.left < 1000:
            self.crossRect.left -= self.dX
        #if self.crossRect.top > -200:
            #self.crossRect.top -= self.dY

    def reset_pos(self):
        self.crossRect.left = 800 + self.crossSurf.get_width()
        self.crossRect.top = 300 - (self.crossSurf.get_height()/2)
        
#EXPERIMENT
class experiment:

    def __init__(self):
        #META
        self._running = True

        #DISPLAY
        self._surf_display = None
        self.size = self.width, self.height = 800,600
        self.titleFont = None
        self.basicFont = None
        self.winFont = None

        #CONTROL
        self.cursorPos = 0,0
        self.mouseButton = 0

        #TIME
        self.numFrames = 0

        #WORLD
        self.worldSize = self.size
        
        #GRAPHICS
  
        #set up world surface
        self.worldSurf = pygame.Surface(self.worldSize)
        self.worldSurf.set_colorkey(TRANSPARENT)

        #set up fonts
        self.smallFont = pygame.font.SysFont(None, 24)
        self.mediumFont = pygame.font.SysFont(None, 36)
        self.bigFont = pygame.font.SysFont(None, 72)

        #'L's and 'T's
        #2 black L's, 2 white L's, 2 black T's, and 2 white T's

        self.whiteL = pygame.Surface((72,72))
        self.whiteL.fill(TRANSPARENT)
        pygame.draw.line(self.whiteL, WHITE, (7,0), (7,72), 16)
        pygame.draw.line(self.whiteL, WHITE, (0,64), (72,64), 16)
        
        self.blackL = pygame.Surface((72,72))
        self.blackL.fill(TRANSPARENT)
        pygame.draw.line(self.blackL, BLACK, (7,0), (7,72), 16)
        pygame.draw.line(self.blackL, BLACK, (0,64), (72,64), 16)
        
        self.whiteT = pygame.Surface((72,72))
        self.whiteT.fill(TRANSPARENT)
        pygame.draw.line(self.whiteT, WHITE, (0,7), (72,7), 16)
        pygame.draw.line(self.whiteT, WHITE, (36,0), (36,72), 16)
        
        self.blackT = pygame.Surface((72,72))
        self.blackT.fill(TRANSPARENT)
        pygame.draw.line(self.blackT, BLACK, (0,7), (72,7), 16)
        pygame.draw.line(self.blackT, BLACK, (36,0), (36,72), 16)


        self.textL1 = letter(self.whiteL)
        self.textL2 = letter(self.whiteL)

        self.textL3 = letter(self.blackL)
        self.textL4 = letter(self.blackL)

        self.textT1 = letter(self.whiteT)
        self.textT2 = letter(self.whiteT)

        self.textT3 = letter(self.blackT)
        self.textT4 = letter(self.blackT)
        
        #gray cross

        #self.crossImg = None
        self.crossImg = pygame.Surface((72,72))
        self.crossImg.fill(TRANSPARENT)
        self.crossImg.set_colorkey(TRANSPARENT)
        pygame.draw.line(self.crossImg, SILVER, (36,0), (36,72), 16)
        pygame.draw.line(self.crossImg, SILVER, (0,36), (72,36), 16)

        self.grayCross = Cross(self.crossImg)
        
    def startUp(self):
        #set window caption
        pygame.display.set_caption('_')
        #kickstart the loop
        self._running = True

        #set up display surface
        self._surf_display = pygame.display.set_mode(self.size, DOUBLEBUF)
        
        #fill display surface with color (probably won't be seen)
        self._surf_display.fill(BLUE)


    def eventmanager(self, event):
        global inputBuffer
        if event.type == pygame.QUIT:
            self._running = False
        #if event.type == timeOut:
        #    self._running = False
        #check for keypresses    
        elif event.type == KEYDOWN:
            #if the player presses Esc, quit
            if event.key == K_ESCAPE:
                self._running = False
            if event.key == K_RETURN:
                if (uR.getUR() == 1):# and (inputBuffer == 'y')):
                    if (inputBuffer == 'y') or (inputBuffer == 'yes'):
                        uR.incUR()
                    else:
                        uR.setUR(3)
                else:
                    uR.incUR()
                #uR.incUR()
                dataFile.writerow([inputBuffer])
                inputBuffer = str()
            if event.key == K_0:
                inputBuffer = inputBuffer + '0'
            if event.key == K_1:
                inputBuffer = inputBuffer + '1'
            if event.key == K_2:
                inputBuffer = inputBuffer + '2'
            if event.key == K_3:
                inputBuffer = inputBuffer + '3'
            if event.key == K_4:
                inputBuffer = inputBuffer + '4'
            if event.key == K_5:
                inputBuffer = inputBuffer + '5'
            if event.key == K_6:
                inputBuffer = inputBuffer + '6'
            if event.key == K_7:
                inputBuffer = inputBuffer + '7'
            if event.key == K_8:
                inputBuffer = inputBuffer + '8'
            if event.key == K_9:
                inputBuffer = inputBuffer + '9'

            if event.key == K_SPACE:
                inputBuffer = inputBuffer + ' '
            if event.key == K_a:
                inputBuffer = inputBuffer + 'a'
            if event.key == K_b:
                inputBuffer = inputBuffer + 'b'
            if event.key == K_c:
                inputBuffer = inputBuffer + 'c'
            if event.key == K_d:
                inputBuffer = inputBuffer + 'd'
            if event.key == K_e:
                inputBuffer = inputBuffer + 'e'
            if event.key == K_f:
                inputBuffer = inputBuffer + 'f'
            if event.key == K_g:
                inputBuffer = inputBuffer + 'g'
            if event.key == K_h:
                inputBuffer = inputBuffer + 'h'
            if event.key == K_i:
                inputBuffer = inputBuffer + 'i'
            if event.key == K_j:
                inputBuffer = inputBuffer + 'j'
            if event.key == K_k:
                inputBuffer = inputBuffer + 'k'
            if event.key == K_l:
                inputBuffer = inputBuffer + 'l'
            if event.key == K_m:
                inputBuffer = inputBuffer + 'm'
            if event.key == K_n:
                inputBuffer = inputBuffer + 'n'
            if event.key == K_o:
                inputBuffer = inputBuffer + 'o'
            if event.key == K_p:
                inputBuffer = inputBuffer + 'p'
            if event.key == K_q:
                inputBuffer = inputBuffer + 'q'
            if event.key == K_r:
                inputBuffer = inputBuffer + 'r'
            if event.key == K_s:
                inputBuffer = inputBuffer + 's'
            if event.key == K_t:
                inputBuffer = inputBuffer + 't'
            if event.key == K_u:
                inputBuffer = inputBuffer + 'u'
            if event.key == K_v:
                inputBuffer = inputBuffer + 'v'
            if event.key == K_w:
                inputBuffer = inputBuffer + 'w'
            if event.key == K_x:
                inputBuffer = inputBuffer + 'x'
            if event.key == K_y:
                inputBuffer = inputBuffer + 'y'
            if event.key == K_z:
                inputBuffer = inputBuffer + 'z'                
                    
    def runTrialNorm(self):
        
        #Draw background
        self.worldSurf.fill(GRAY)
        self._surf_display.blit(self.worldSurf,(((self.worldSize[0]/2)-(self.size[0]/2)),((self.worldSize[1]/2)-(self.size[1]/2))))
               
        #Wipe the screen
        self.worldSurf.fill(GRAY)
        
        #render world to screen
        self._surf_display.blit(self.worldSurf, (0,0))#, ((self.size[0]/2),(self.size[1]/2)))

##DELETE WHEN DONE WITH THIS:
##All of the events on each trial took
##place within a gray 12.7 x 15.5 cm display window (luminance = 32.1 cd/m2).
##Within this window, four black (luminance = 1.2 cd/m2) and four
##white (luminance = 88.0 cd/m2) L and T shapes (1 cm x 1 cm block letters) each moved
##independently on random paths at a variable rate ranging from 2-5 cm/s. Their range of
##motion extended from 5.5 cm above the horizontal line to 5.5 cm below the horizontal
##line, a region occupying 87% of the vertical extent of the display window, and the objects
##were smoothly repulsed as they approached the limits of this region. The objects could
##occlude each other as they passed. Periodically, each black and white shape "touched" the
##horizontal line, usually as it crossed from one half of the display to the other. Each trial
##lasted for a total of 15 seconds, and each observer completed 5 trials.

        #display blue center line
        
        pygame.draw.line(self._surf_display, NAVY, (0,(self.height/2)), (((self.width/2)-8),(self.height/2)), 1)
        pygame.draw.line(self._surf_display, NAVY, (((self.width/2)+8),(self.height/2)), (self.width,(self.height/2)), 1)

        #display fixation point (a small blue box - see Most et al. 2000)

        pygame.draw.rect(self._surf_display, NAVY, ((392,292),(16,16)), 4)

        #move L's and T's    
        self.textL1.update()
        self.textL2.update()
        self.textL3.update()
        self.textL4.update()

        self.textT1.update()
        self.textT2.update()
        self.textT3.update()
        self.textT4.update()
        
        #display L's and T's
        self.textL1.draw(self._surf_display)
        self.textL2.draw(self._surf_display)
        self.textL3.draw(self._surf_display)
        self.textL4.draw(self._surf_display)

        self.textT1.draw(self._surf_display)
        self.textT2.draw(self._surf_display)
        self.textT3.draw(self._surf_display)
        self.textT4.draw(self._surf_display)
                
        #lastly, flip the display        
        pygame.display.flip()

    def runTrialAlt(self):

        #keep track of the number of frames - needed to move the gray cross at a slow enough rate
        self.numFrames += 1 
        
        #Draw background
        self.worldSurf.fill(GRAY)
        self._surf_display.blit(self.worldSurf,(((self.worldSize[0]/2)-(self.size[0]/2)),((self.worldSize[1]/2)-(self.size[1]/2))))
               
        #Wipe the screen
        self.worldSurf.fill(GRAY)
        
        #render world to screen
        self._surf_display.blit(self.worldSurf, (0,0))#, ((self.size[0]/2),(self.size[1]/2)))

        #display blue center line
        
        pygame.draw.line(self._surf_display, NAVY, (0,(self.height/2)), (((self.width/2)-8),(self.height/2)), 1)
        pygame.draw.line(self._surf_display, NAVY, (((self.width/2)+8),(self.height/2)), (self.width,(self.height/2)), 1)

        #display fixation point (a small blue box - see Most et al. 2000)

        pygame.draw.rect(self._surf_display, NAVY, ((392,292),(16,16)), 4)

        #move L's and T's    
        self.textL1.update()
        self.textL2.update()
        self.textL3.update()
        self.textL4.update()

        self.textT1.update()
        self.textT2.update()
        self.textT3.update()
        self.textT4.update()
       
        #display L's and T's
        self.textL1.draw(self._surf_display)
        self.textL2.draw(self._surf_display)
        self.textL3.draw(self._surf_display)
        self.textL4.draw(self._surf_display)

        self.textT1.draw(self._surf_display)
        self.textT2.draw(self._surf_display)
        self.textT3.draw(self._surf_display)
        self.textT4.draw(self._surf_display)

        #move gray cross, starting at 5 seconds
        if self.numFrames >= 150:
            self.grayCross.update()
            #display gray cross
            self.grayCross.draw(self._surf_display)

        #TEMP - TESTING ONLY - display gray cross
        #self._surf_display.blit(self.crossImg, (-36,564))
        
        #lastly, flip the display        
        pygame.display.flip()

    def promptUser(self, Prompt):
        self._surf_display.fill(BLACK)
        self.textPrompt = self.mediumFont.render(Prompt, True, WHITE, TRANSPARENT)
        self.textPrompt.set_colorkey(TRANSPARENT)
        self.textRect = self.textPrompt.get_rect()
        self.textRect.left = 400 - (self.textPrompt.get_width() / 2)
        self.textRect.top = 250
        self._surf_display.blit(self.textPrompt, self.textRect)

        self.textResponse = self.mediumFont.render(inputBuffer, True, LIME, TRANSPARENT)
        self.textResponse.set_colorkey(TRANSPARENT)
        self.textResponseRect = self.textResponse.get_rect()
        self.textResponseRect.left = 400 - (self.textResponse.get_width() / 2)
        self.textResponseRect.top = 300
        self._surf_display.blit(self.textResponse, self.textResponseRect)

        pygame.display.flip()

    def readyScreen(self, waitTime):

        #this line is a kludge to get the program working
        self._surf_display = pygame.display.set_mode(self.size, DOUBLEBUF)
                
        self._surf_display.fill(BLACK)
        self.textPrompt = self.bigFont.render('READY', True, WHITE, TRANSPARENT)
        self.textPrompt.set_colorkey(TRANSPARENT)
        self.textRect = self.textPrompt.get_rect()
        self.textRect.left = 400 - (self.textPrompt.get_width() / 2)
        self.textRect.top = 250
        self._surf_display.blit(self.textPrompt, self.textRect)
        
        pygame.display.flip()
        pygame.time.wait(waitTime)

    def cleanup(self):
        pygame.quit()

    def on_execute(self, FPS, trialFlag):
        if self.startUp() == False:
            self._running = False
        #set up clock
        clock = pygame.time.Clock()
        FRAMES_PER_SECOND = FPS
        countDown = 450
        self.numFrames = 0
        self.grayCross.reset_pos()
        
        while(self._running):
            #advance the clock 1 tick
            clock.tick(FRAMES_PER_SECOND)    
            for event in pygame.event.get():
                self.eventmanager(event)
            if trialFlag == 1:
                self.runTrialNorm()
            elif trialFlag == 2:
                self.runTrialAlt()
            countDown = countDown - 1
            if countDown == 0:
                self._running = False

    def on_transition(self, FPS, trialFlag):
        if self.startUp() == False:
            self._running = False
        #set up clock
        clock = pygame.time.Clock()
        FRAMES_PER_SECOND = FPS
        uR.setUR(0)
        
        if trialFlag == 1:
            while(self._running):
                #advance the clock 1 tick
                clock.tick(FRAMES_PER_SECOND)    
                for event in pygame.event.get():
                    self.eventmanager(event)
                if uR.getUR() == 0:
                    self.promptUser('How many times did the black shapes cross the center line?')
                elif uR.getUR() == 1:
                    self._running = False
                    
        elif trialFlag == 2:
            while(self._running):
                #advance the clock 1 tick
                clock.tick(FRAMES_PER_SECOND)    
                for event in pygame.event.get():
                    self.eventmanager(event)
                if uR.getUR() == 0:
                    self.promptUser('How many times did the black shapes cross the center line?')
                elif uR.getUR() == 1:
                    self.promptUser('Did you notice anything unusual?')
                elif uR.getUR() == 2:
                    self.promptUser('What was it?')
                elif uR.getUR() == 3:
                    self._running = False
                    

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":

    #create instance of game
    inBlind = experiment()
    #show ready screen for 5 seconds (5000 ms)
    inBlind.readyScreen(5000)
    #execute the game loop at 30 FPS
    #Trial 0 (practice trial)
    inBlind.on_execute(30,1)
    inBlind.on_transition(30,1)
    #Trial 1
    userResponse = 0
    inBlind.on_execute(30,1)
    inBlind.on_transition(30,1)
    #Trial 2
    userResponse = 0
    inBlind.on_execute(30,1)
    inBlind.on_transition(30,1)
    #Trial 3
    userResponse = 0
    inBlind.on_execute(30,2)
    inBlind.on_transition(30,2)
    #Trial 4
    userResponse = 0
    inBlind.on_execute(30,2)
    inBlind.on_transition(30,2)
    #Trial 5
    userResponse = 0
    inBlind.on_execute(30,2)
    inBlind.on_transition(30,2)

    #Close data file and quit
    dataFilename.close()
    pygame.quit()

#this is a simple program to process the data from the remember/know task

import pygame, math, sys, csv, random
from pygame.locals import *

novelList = list()
familiarList = list()
comboList = list()
studiedList = list()
unstudiedList = list()

falsePos = 0
trueNeg = 0
positives = 0
negatives = 0
remembered = 0
known = 0
guessed = 0
remTot = 0
knownTot = 0
guessTot = 0
rownum = 0
colnum = 0
conTot = 0
conCorrTot = 0

partNum = input('Please enter participant #: ')

fileName = 'dataLog_p2_' + str(partNum) + '.csv' #input('Please enter the filename:')

c = csv.reader(open(fileName, 'rb'), delimiter = ',', quotechar='|')
t = csv.reader(open(fileName, 'rb'), delimiter = ',', quotechar='|')

novelWords = csv.reader(open('wordset3.txt', 'rb'), delimiter=',', quotechar='|')
for row in novelWords:
    novelList.append(row)
##    for col in novelWords:
##        novelList.append(col)
##        #print col

familiarWords = csv.reader(open('wordsetcombo12.txt', 'rb'), delimiter=',', quotechar='|')
for row in familiarWords:
    familiarList.append(row)
##    for col in familiarWords:
##        familiarList.append(col)

for row in c:
    rownum += 1
    colnum = 0
    for col in row:
        colnum += 1
        if (rownum > 240) and (rownum <= 280):
            if colnum == 1:
                #print col #TEST ONLY
                studiedList.append(col)

rownum = 0
famNum = 0
novelNum = 0
famTot = 0
novelTot = 0
novelMiss = 0
famMiss = 0

for row in t:
    rownum += 1
    colnum = 0
    checkFlag = 0
    familiarFlag = 0
    novelFlag = 0

    for col in row:  
        colnum += 1
        if (rownum > 280):
            #print str(rownum) + ':' + str(colnum) #TEST ONLY
            if colnum == 1:
                comboList.append(col)
                for d in studiedList:
                    if col == d:
                        #print col
                        checkFlag = 1
                for e in familiarList:
                    if ('[\'' + str(col) + '\']') == str(e):
                        #print str(col)
                        famTot += 1
                        familiarFlag = 1
                for f in novelList:
                    #print f
                    #novelTot += 1
                    if ('[\'' + str(col) + '\']') == str(f):
                        #print str(col)
                        novelTot += 1
                        novelFlag = 1
            if (colnum == 2) and (checkFlag == 0):
                if col == 'green':
                    falsePos += 1
                    if familiarFlag == 1:
                        famMiss += 1
                        #print 'familiar'
                        familiarFlag = 0
                    elif novelFlag == 1:
                        novelMiss += 1
                        #print 'novel'
                        novelFlag = 0
                    else:
                        novelMiss += 1
                if col == 'red':
                    trueNeg += 1
            if (colnum == 2) and (checkFlag == 1):
                if col == 'green':
                    positives += 1
                    if familiarFlag == 1:
                        famNum += 1
                        #print 'familiar'
                        familiarFlag = 0
                    if novelFlag == 1:
                        novelNum += 1
                        #print 'novel'
                        novelFlag = 0
                if col == 'red':
                    negatives += 1
                    checkFlag = 0
            if colnum == 3:
                if col == 'c3':
                    conTot += 3
                    remTot += 1
                if col == 'c2':
                    conTot += 2
                    knownTot += 1
                if col == 'c1':
                    conTot += 1
                    guessTot += 1
                if checkFlag == 1:
                    if col == 'c3':
                        remembered += 1
                    if col == 'c2':
                        known += 1
                    if col == 'c1':
                        guessed += 1 

remTot = remTot + remembered
knownTot = knownTot + known
guessTot = guessTot + guessed

conCorrTot = (remembered*3)+(known*2)+guessed

print ''
print 'Data for participant #' + str(partNum)
print '--------------------------'

#print 'novel total: ' + str(novelTot)
#print 'familiar total: ' + str(famTot)

print 'positives: ' + str(positives)
print 'false positives (false alarms): ' + str(falsePos)
print 'true negatives: ' + str(trueNeg)
print 'negatives (incorrect): ' + str(negatives)
print 'confidence score for correctly remembered words (best is 120): ' + str(conCorrTot)
print 'total confidence score (max 240): ' + str(conTot)
print '% confidence for correctly remembered / 120): ' + str((float(conCorrTot)/120)*100)
print 'mean confidence score (for correctly remembered words): ' + str(float(conCorrTot)/(remembered + known + guessed))
print 'mean confidence score (for all words): ' + str(float(conTot)/(remTot + knownTot + guessTot))
print 'accuracy (% of positives/total) : ' + str(float(positives)/40*100)
print 'accuracy (% of true positives and negatives/80): ' + str((float(positives + trueNeg)/80)*100)
print '# remembered: ' + str(remembered)
print '# known: ' + str(known)
print '# guessed: ' + str(guessed)
print ''
print 'novel items correct (novel hit rate - false alarms): ' + str(novelNum)
print 'familiar items correct: (familiar hit rate - false alarms)' + str(famNum)
print 'ratio of novel to familiar words (correct): ' + str(float(novelNum)/float(famNum)) 
print 'number of test words judged to be critical study list words (novel): ' + str(novelNum + novelMiss)
print 'number of test words judged to be critical study list words (familiar): ' + str(famNum + famMiss) 
print 'proportion of test words judged to be critical study-list words (novel): ' + str(float(novelNum + novelMiss)/80)
print 'proportion of test words judged to be critical study-list words (familiar): ' + str(float(famNum + famMiss)/80)

outputFilename = open('result_p2.csv', 'ab')
outputFile = csv.writer(outputFilename, delimiter = ',', quotechar='|')
outputFile.writerow([partNum,positives,falsePos,trueNeg,negatives,conCorrTot,conTot,remembered,known,guessed,remTot,knownTot,guessTot,novelNum,famNum,novelMiss,famMiss])

outputFilename.close()

#Tulving and Kroll's (1995) hypothesis: hit rate(novel) - false alarms(novel) > hit rate(familiar) - false alarms(familiar)
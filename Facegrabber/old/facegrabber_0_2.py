#Facegrabber 0.1 by Ted Lougheed - Image capture and automatic pasting utility
#Last updated: 2-Mar-2012
#
#This program is designed for a very specific use in a cognitive psychology
#experiment. It captures an image from a webcam (NOT YET IMPLEMENTED - will
#use VideoCapture libraries) and pastes it at specific locations in target files (using the
#pygame libraries), specified in a CSV file that the user can edit.
#The program is intended to take a picture of a research participant and
#superimpose that image over the image of the protagonist in a series of
#scenario images. It can easily be repurposed to do batch pasting jobs of
#any variety.

import pygame, math, sys, csv
import numpy, scipy, cv
sys.path.append("C:\OpenCV2.2\Python2.7\Lib\site-packages")
import cv
from pygame.locals import *
from VideoCapture import *

#initialize pygame module
pygame.init()

#define main function
def main():
    
    #define cursor position
    cursorPos = 0,0

    #set up colors
    TRANSPARENT = (0,0,1)
    BLACK = (0,0,0)
    WHITE = (255,255,255)
    RED = (255,0,0)
    GREEN = (0,255,0)
    BLUE = (0,0,255)
    DARKBLUE = (0,0,127)
    LIGHTBLUE = (127,127,255)

    #initialize camera
    cam = Device()

    #set cam activation flag to false
    camActive = False

    #set up window
    #set window caption
    pygame.display.set_caption('Facegrabber 0.2')
    #load and set logo
    #create the main surface on the screen
    screenWidth = 640
    screenHeight = 480
    screen = pygame.display.set_mode((screenWidth, screenHeight), DOUBLEBUF)
    pygame.display.flip()
  
    #set up fonts
    titleFont = pygame.font.SysFont(None, 16)
    basicFont = pygame.font.SysFont(None, 12)

    #set up clock
    clock = pygame.time.Clock()
    FRAMES_PER_SECOND = 30

    #set up text (temp)
    text = titleFont.render('Facegrabber 0.2', True, WHITE, BLACK)
    textRect = text.get_rect()
    textRect.left = 16
    textRect.top = 16

    #set up capture ellipse image
    #imgEllipse = pygame.Surface((180,200))
    #imgEllipse.fill(BLACK)
    #imgEllipse.set_colorkey(TRANSPARENT)
    #pygame.draw.ellipse(imgEllipse, RED, (0,0,180,200), 1)
    #pygame.draw.ellipse(imgEllipse, TRANSPARENT, (0,0,180,200), 0)

    imgSelect = pygame.Surface((180,200))
    imgSelect.set_colorkey(BLACK)
    pygame.draw.ellipse(imgSelect, RED, (0,0,180,200), 1)

    #OLD set up world surface
    #worldWidth = 800
    #worldHeight = 600
    #worldSurf = pygame.Surface((worldWidth,worldHeight))
    
    #draw background
    screen.fill(BLACK)
    #OLD worldSurf.fill(BLACK)
    #OLD screen.blit(worldSurf,(((worldWidth/2)-(screenWidth/2)),((worldHeight/2)-(screenHeight/2))))
            
    #define a variable to control the main loop
    running = True

    ##TESTING CAMERA CAPTURE

    #grab a picture from the camera
    partNum = raw_input('Enter participant #:')
    fileName = 'part' + partNum + '.png'
    faceGrab = pygame.Surface((180,200))
    
    #DOESN'T WORK cam.setResolution(800,600)
    #cam.saveSnapshot(fileName, timestamp=1)

    #Show the captured image on screen

    #set up capture ellipse image
    imgEllipse = pygame.Surface((180,200))
    imgEllipse.fill(GREEN)
    imgEllipse.set_colorkey(TRANSPARENT)
    pygame.draw.ellipse(imgEllipse, TRANSPARENT, (0,0,180,200), 0)
    
    #camSnap = pygame.image.load(fileName)
    #camSnapRect = camSnap.get_rect()
    #screen.blit(camSnap, (0,0))
    
    pygame.display.flip()

    #TEMP load input image
    #inputImgName = raw_input('Enter the name of the input image to load: ')
    #inputImg = pygame.image.load(inputImgName)
    #inputImgRect = inputImg.get_rect()

    #main loop
    while running:
        #advance the clock 1 tick
        clock.tick(FRAMES_PER_SECOND)

        ##TEMP pygame.camera.list_cameras()

        #USER_INPUT
        #event handling, gets all events from the eventqueue
        for event in pygame.event.get():
            #if the user clicks the X, quit
            if event.type == QUIT:
                #change value to false, exit main loop
                running = False
              #  sys.exit()
            if event.type == MOUSEMOTION:
                cursorPos = event.pos
            if not hasattr(event, 'key'): continue
            #exit loop if the player presses Esc
            if (event.key == K_ESCAPE):
                running = False
            #capture image from camera
            if (event.key == K_SPACE):
                cam.saveSnapshot(fileName, timestamp=1)
                camSnap = pygame.image.load(fileName)
                camActive = True
            #paste face onto scenario backdrops
            if (event.type == KEYUP):
                if (event.key == K_p):
                    #load csv file containing pasting positions for each scenario file
                    csvFilename = raw_input('Enter the name of the index file (CSV format): ')
                    posReader = csv.reader(open(csvFilename, 'rb'), delimiter=',', quotechar='|')

                    #load participant image
                    inputImgNum = raw_input('Enter the participant #: ')
                    inputImg = pygame.image.load('facegrab' + inputImgNum + '.png')
                    inputImg.set_colorkey(GREEN)
                    inputImgRect = inputImg.get_rect()

                    #iterate through the list of files and positions, pasting the input image onto the background image in the right location
                    rownum = 0
                    for row in posReader:
                        colnum = 0
                        if rownum > 0:
                            for col in row:
                                if colnum == 0:
                                    outputFilename = col
                                    backdrop = pygame.image.load(col)
                                elif colnum == 1:
                                    xpos = int(col)
                                elif colnum == 2:
                                    ypos = int(col)
                                colnum +=1
                            ##DEBUG print '{}, {}'.format(xpos, ypos)
                            #display backdrop image
                            screen.blit(backdrop, (0,0))
                            #display input image (centered on positions defined in csv file) on top of background image
                            screen.blit(inputImg, ((xpos - inputImgRect.centerx),(ypos - inputImgRect.centery)))
                            #flip the display
                            pygame.display.flip()
                            #save the new image over the old backdrop image
                            pygame.image.save(screen, outputFilename)            
                        rownum += 1

            #save snapshot of screen
            if (event.key == K_s):
                pygame.image.save(screen, 'img.png') #TEMP
            #grab selected face
            if (event.type == KEYUP):
                if (event.key == K_c):
                    screen.blit(camSnap, (0,0))
                    #pygame.draw.rect(screen, GREEN, (cursorPos[0],cursorPos[1],cursorPos[0]-180,cursorPos[1]-200), 4)#TESTING ONLY
                    faceGrab.blit(screen, (-(cursorPos[0]),-(cursorPos[1]),180,200))
                                    
                    faceGrab.blit(imgEllipse, (0,0))
                    pygame.image.save(faceGrab, 'facegrab' + partNum + '.png')#NOT WORKING YET
                    print('face grabbed')
                    print(str(cursorPos[0]) + ', ' + str(cursorPos[1]))
                    
        #RENDER    
        #if image has been captured from the camera, show it on screen.
        if (camActive == True):
            screen.blit(camSnap, (0,0))

            #draw ellipse indicating area of capture
            screen.blit(imgSelect, cursorPos)

        #lastly, flip the display
        pygame.display.flip()

    #If the running flag is false, quit the program.
    if running == False:
        pygame.quit()

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":
    #call main function
    main()

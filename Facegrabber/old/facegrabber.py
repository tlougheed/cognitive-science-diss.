#Facegrabber 0.1 by Ted Lougheed - Image capture and automatic pasting utility
#Last updated: 2-Feb-2012
#
#This program is designed for a very specific use in a cognitive psychology
#experiment. It captures an image from a webcam (NOT YET IMPLEMENTED - will
#use OpenCV) and pastes it at specific locations in target files (using the
#pygame libraries), specified in a CSV file that the user can edit.
#The program is intended to take a picture of a research participant and
#superimpose that image over the image of the protagonist in a series of
#scenario images. It can easily be repurposed to do batch pasting jobs of
#any variety.

import pygame, math, sys, csv
from pygame.locals import *

#define main function
def main():
    
    #define cursor position
    cursorPos = 0,0

    #set up colors
    BLACK = (0,0,0)
    WHITE = (255,255,255)
    RED = (255,0,0)
    GREEN = (0,255,0)
    BLUE = (0,0,255)
    DARKBLUE = (0,0,127)
    LIGHTBLUE = (127,127,255)

    #initialize pygame module
    pygame.init()

    #set up window
    #set window caption
    pygame.display.set_caption('Facegrabber 0.1')
    #load and set logo
    #create the main surface on the screen
    screenWidth = 800
    screenHeight = 600
    screen = pygame.display.set_mode((screenWidth, screenHeight), DOUBLEBUF)
    pygame.display.flip()
  
    #set up fonts
    titleFont = pygame.font.SysFont(None, 16)
    basicFont = pygame.font.SysFont(None, 12)

    #set up clock
    clock = pygame.time.Clock()
    FRAMES_PER_SECOND = 30

    #set up text (temp)
    text = titleFont.render('Facegrabber Demo', True, WHITE, BLACK)
    textRect = text.get_rect()
    textRect.left = 16
    textRect.top = 16

    #set up world surface
    worldWidth = 800
    worldHeight = 600
    worldSurf = pygame.Surface((worldWidth,worldHeight))
    #i'll have to change this later, since where the worldSurf
    #appears will depend on where the player spawns.
    #Once multiplayer is in place, it will be given by the server
    
    #draw background
    screen.fill(BLUE)
    worldSurf.fill(BLACK)
    screen.blit(worldSurf,(((worldWidth/2)-(screenWidth/2)),((worldHeight/2)-(screenHeight/2))))
        
    #define a variable to control the main loop
    running = True

    #load input image
    inputImgName = raw_input('Enter the name of the input image to load: ')
    inputImg = pygame.image.load(inputImgName)
    inputImgRect = inputImg.get_rect()

    #load csv file containing pasting positions for each scenario file
    csvFilename = raw_input('Enter the name of the index file (CSV format): ')
    posReader = csv.reader(open(csvFilename, 'rb'), delimiter=',', quotechar='|')

    #iterate through the list of files and positions, pasting the input image onto the background image in the right location
    rownum = 0
    for row in posReader:
        colnum = 0
        if rownum > 0:
            for col in row:
                if colnum == 0:
                    outputFilename = col
                    backdrop = pygame.image.load(col)
                elif colnum == 1:
                    xpos = int(col)
                elif colnum == 2:
                    ypos = int(col)
                colnum +=1
            ##DEBUG print '{}, {}'.format(xpos, ypos)
            #display backdrop image
            screen.blit(backdrop, (0,0))
            #display input image (centered on positions defined in csv file) on top of background image
            screen.blit(inputImg, ((xpos - inputImgRect.centerx),(ypos - inputImgRect.centery)))
            #flip the display
            pygame.display.flip()
            #save the new image over the old backdrop image
            pygame.image.save(screen, outputFilename)            
        rownum += 1

    #main loop
    while running:
        #advance the clock 1 tick
        clock.tick(FRAMES_PER_SECOND)

        ##TEMP pygame.camera.list_cameras()

        #USER_INPUT
        #event handling, gets all events from the eventqueue
        for event in pygame.event.get():
            #if the user clicks the X, quit
            if event.type == QUIT:
                #change value to false, exit main loop
                running = False
              #  sys.exit()
            if event.type == MOUSEMOTION:
                cursorPos = event.pos
            if not hasattr(event, 'key'): continue
            #exit loop if the player presses Esc
            if (event.key == K_ESCAPE):
                running = False
            #save snapshot of screen
            if (event.key == K_s):
                pygame.image.save(screen, 'img.png') #TEMP

    if running == False:
        pygame.quit()

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":
    #call main function
    main()


##TO DO: comment out test code and uncomment final code

import pygame, math, random, sys, os
from pygame.locals import *

#define color words
TRANSPARENT = (0,0,1) #set aside almost-black for use as colorkey
BLACK = (0,0,0)
WHITE = (255,255,255)
SILVER = (192,192,192)
GRAY = (128,128,128)
RED = (255,0,0)
MAROON = (128,0,0)
YELLOW = (255,255,0)
OLIVE = (128,128,0)
LIME = (0,255,0)
GREEN = (0,128,0)
AQUA = (0,255,255)
TEAL = (0,128,128)
BLUE = (0,0,255)
NAVY = (0,0,128)
FUCHSIA = (255,0,255)
PURPLE = (128,0,128)
SUNSET = (255,128,128)
ORANGE = (255,128,0)
LIGHTGREEN = (128, 255, 128)
UN_BLUE = (128,128,255)
AZURE = (0,128,255)

#initiate pygame
pygame.init()

#UNCOMMENT WHEN DONE TESTING:
#nInput1 = int(input('Enter a random 3-digit number (First trial): '))
#nInput2 = int(input('Enter a random 3-digit number (Second trial): '))
#nInput3 = int(input('Enter a random 3-digit number (Third trial): '))
#nInput4 = int(input('Enter a random 3-digit number (Fourth trial): '))

#SOUND
pygame.mixer.init()

#TESTING ONLY:
Sound1 = pygame.mixer.Sound('845.wav')
Sound2 = pygame.mixer.Sound('473.wav')
Sound3 = pygame.mixer.Sound('977.wav')
Sound4 = pygame.mixer.Sound('306.wav')

#UNCOMMENT WHEN DONE TESTING:
#Sound1 = pygame.mixer.Sound(str(nInput1) + '.wav')
#Sound2 = pygame.mixer.Sound(str(nInput2) + '.wav')
#Sound3 = pygame.mixer.Sound(str(nInput3) + '.wav')
#Sound4 = pygame.mixer.Sound(str(nInput4) + '.wav')

#EXPERIMENT
class experiment:
    
    def __init__(self):
        #META
        self._running = True

        #DISPLAY
        self._surf_display = None
        self.size = self.width, self.height = 1366,768
        self.titleFont = None
        self.basicFont = None

        #CONTROL
        self.cursorPos = 0,0
        self.mouseButton = 0

        #TIMING
        self.ReadyFlag = 0
        self.TimeSinceStart = None
        self.trialNum = 0 #Trial number. Starts at 0 but is increased by 1 at the beginning of each trial

        #WORLD
        self.worldSize = self.size

        #SOUND
        self.PlayNumber = 0
        
        #GRAPHICS
  
        #set up world surface
        self.worldSurf = pygame.Surface(self.worldSize)
        self.worldSurf.set_colorkey(TRANSPARENT)

        #set up fonts
        self.basicFont = pygame.font.SysFont(None, 24)
        self.titleFont = pygame.font.SysFont(None, 72)
                
        #set up text

        self.textStop = self.titleFont.render('STOP', True, WHITE, TRANSPARENT)
        self.textStop.set_colorkey(TRANSPARENT)
        self.textStopRect = self.textStop.get_rect()
        self.textStopRect.left = 600
        self.textStopRect.top = 350

        self.textReady = self.titleFont.render('READY', True, WHITE, TRANSPARENT)
        self.textReady.set_colorkey(TRANSPARENT)
        self.textReadyRect = self.textReady.get_rect()
        self.textReadyRect.left = 600
        self.textReadyRect.top = 350

        #set up images

        #set top left corner of scenario images (will have to be modified if different-sized images are used
        self.imgPos = (350,150)

        self.imgCross = pygame.Surface((64,64))
        self.imgCross.set_colorkey(TRANSPARENT)
        pygame.draw.line(self.imgCross, WHITE, (32,0), (32,64), 3)
        pygame.draw.line(self.imgCross, WHITE, (0,32), (64,32), 3)

        #TESTING ONLY
        self.inputImg1_1 = pygame.image.load('backdrop.png')
        self.inputImg1_2 = pygame.image.load('backdrop2.png')
        self.inputImg1_3 = pygame.image.load('backdrop3.png')

        self.inputImg2_1 = pygame.image.load('backdrop4.png')
        self.inputImg2_2 = pygame.image.load('backdrop2.png')
        self.inputImg2_3 = pygame.image.load('backdrop3.png')

        self.inputImg3_1 = pygame.image.load('backdrop3.png')
        self.inputImg3_2 = pygame.image.load('backdrop4.png')
        self.inputImg3_3 = pygame.image.load('backdrop.png')

        self.inputImg4_1 = pygame.image.load('backdrop2.png')
        self.inputImg4_2 = pygame.image.load('backdrop3.png')
        self.inputImg4_3 = pygame.image.load('backdrop4.png')

        #UNCOMMENT WHEN DONE TESTING:
        ##self.inputImg1_1 = pygame.image.load('back1_1.png')
        ##self.inputImg1_2 = pygame.image.load('back1_2.png')
        ##self.inputImg1_3 = pygame.image.load('back1_3.png')
        ##
        ##self.inputImg2_1 = pygame.image.load('back2_1.png')
        ##self.inputImg2_2 = pygame.image.load('back2_2.png')
        ##self.inputImg2_3 = pygame.image.load('back2_3.png')
        ##
        ##self.inputImg3_1 = pygame.image.load('back3_1.png')
        ##self.inputImg3_2 = pygame.image.load('back3_2.png')
        ##self.inputImg3_3 = pygame.image.load('back3_3.png')
        ##
        ##self.inputImg4_1 = pygame.image.load('back4_1.png')
        ##self.inputImg4_2 = pygame.image.load('back4_2.png')
        ##self.inputImg4_3 = pygame.image.load('back4_3.png')
        
    def startUp(self):

        #set window caption
        pygame.display.set_caption('_')
        #kickstart the loop
        self._running = True

        #set up display surface
        self._surf_display = pygame.display.set_mode(self.size, FULLSCREEN)
        
        #fill display surface with color (probably won't be seen)
        self._surf_display.fill(BLUE)

    def eventmanager(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        #check for keypresses    
        elif event.type == KEYDOWN:
            #if the player presses Esc, quit
            if event.key == K_ESCAPE:
                self._running = False
            #if the player presses Space while 'READY' is displayed,
            #start the trial.
            elif (event.key == K_SPACE) and (self.ReadyFlag == 0):
                self.ReadyFlag = 1
                self.trialNum = self.trialNum + 1
                self.PlayNumber = self.trialNum
                #self.TimeSinceStart = pygame.time.get_ticks()
                #DEBUG print(self.TimeSinceStart)
                
    def simulation(self):
        pass

    def clearScreen(self):
        self.worldSurf.fill(BLACK)
        self._surf_display.blit(self.worldSurf, (0,0))
        pygame.display.flip()

    def runTrial(self,inpSound,inpImg1,inpImg2,inpImg3):
        ##Procedure for each trial as follows:
        ##display 'READY' on blank for 500 ms
        ##display 2000 ms of blank, play audio recording of number
        ##display 2000 ms of first picture
        ##display 200 ms of blank
        ##display 2000 ms of 2nd picture
        ##display 200 ms of blank
        ##display 2000 ms of 3rd picture
        ##display 200 ms of blank
        ##display 'STOP' on blank, prompt for next trial

        #display 'READY' on blank for 500 ms
        self._surf_display.blit(self.textReady, self.textReadyRect)
        pygame.display.flip()

        pygame.time.delay(500)

        #play audio recording of number
        inpSound.play()
        self.PlayNumber = 0

        #display 2500 ms of blank
        self.clearScreen()
        pygame.time.delay(2500)

        #display 2000 ms of first picture
        self._surf_display.blit(inpImg1, self.imgPos)
        pygame.display.flip()
        pygame.time.delay(2000)

        #display 200 ms of blank
        self.clearScreen()
        pygame.time.delay(200)

        #display 2000 ms of second picture
        self._surf_display.blit(inpImg2, self.imgPos)
        pygame.display.flip()
        pygame.time.delay(2000)

        #display 200 ms of blank
        self.clearScreen()
        pygame.time.delay(200)

        #display 2000 ms of third picture
        self._surf_display.blit(inpImg3, self.imgPos)
        pygame.display.flip()
        pygame.time.delay(2000)

        #display 200 ms of blank
        self.clearScreen()
        pygame.time.delay(200)

        #display 'STOP' on blank, prompt for next trial
        self._surf_display.blit(self.textStop, self.textStopRect)
        pygame.display.flip()
        pygame.time.delay(1000)

        #this is just a short buffer of blank space before the next trial
        self.clearScreen()
        pygame.time.delay(500)

    def on_render(self):
        
        #Wipe the screen
        self.worldSurf.fill(BLACK)

        #render world to screen
        self._surf_display.blit(self.worldSurf, (0,0))#, ((self.size[0]/2),(self.size[1]/2)))

        #RUN TRIALS
        
        #display fixation cross if in between trials (when ReadyFlag is 0)
        if (self.ReadyFlag == 0):
            self._surf_display.blit(self.imgCross, (650,352))
            pygame.display.flip()
        
        if (self.ReadyFlag == 1):
            if (self.PlayNumber == 1):
                self.runTrial(Sound1, self.inputImg1_1, self.inputImg1_2, self.inputImg1_3)
                self.ReadyFlag = 0

            if (self.PlayNumber == 2):
                self.runTrial(Sound2, self.inputImg2_1, self.inputImg2_2, self.inputImg2_3)
                self.ReadyFlag = 0

            if (self.PlayNumber == 3):
                self.runTrial(Sound3, self.inputImg3_1, self.inputImg3_2, self.inputImg3_3)
                self.ReadyFlag = 0

            if (self.PlayNumber == 4):
                self.runTrial(Sound4, self.inputImg4_1, self.inputImg4_2, self.inputImg4_3)
                
        #lastly, flip the display        
        pygame.display.flip()

    def cleanup(self):
        pygame.quit()

    def on_execute(self, FPS):
        if self.startUp() == False:
            self._running = False
        #set up clock
        clock = pygame.time.Clock()
        FRAMES_PER_SECOND = FPS
        
        while(self._running):
            #advance the clock 1 tick
            clock.tick(FRAMES_PER_SECOND)

            for event in pygame.event.get():
                self.eventmanager(event)
            self.simulation()
            self.on_render()
        self.cleanup()

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":

    #create instance of experiment
    backwardCount = experiment()
    #execute the experiment loop at 30 FPS
    backwardCount.on_execute(30)

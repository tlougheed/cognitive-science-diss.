import pygame, math, sys, csv, random
from pygame.locals import *

pygame.init()

pygame.display.set_caption('_')

TRANSPARENT = (152,216,223)
BLACK = (0,0,0)
WHITE = (255,255,255)
RED = (255,0,0)
GREEN = (0,128,0)
LIME = (0,255,0)
BLUE = (0,0,255)
BROWN = (150,75,0)
NEUTRAL_BLUE = (153,216,223)

#QUESTION_COUNT = 5 #OLD - don't use

FRAMERATE = 30

basicFont = pygame.font.SysFont(None, 72)
smallFont = pygame.font.SysFont(None, 36)

#set key repeat parameters
pygame.key.set_repeat(500, 50)

pygame.mouse.set_visible(False)

class experiment:

    def __init__(self):

        self._surf_display = None
        self._surf_display = pygame.display.set_mode((1024,600), FULLSCREEN)
        self._surf_display.fill(NEUTRAL_BLUE)

        self.background = pygame.Surface((1024,600))
        self.background.set_colorkey(TRANSPARENT)
        self.background.fill(NEUTRAL_BLUE)

        self.imgMask = pygame.image.load('whitenoise.jpg')

        self.inputBuffer = ''
        self.isRunning = True
        self.questionNum = 0
        self.questionMax = 0
        
        self.questionList = list()
        self.questionDict = {}

        #list to hold data (keyboard inputs)
        self.responseLog = list()
        self.responseDict = {}

        self.keyList = list()
        self.newList = list()

        self.f = csv.reader(open('questionSet.txt', 'rb'), delimiter=',', quotechar='|')

        #self.trialTimer = 0
        #self.trialMaxTime = 30 #in seconds

    def showMask(self):
        self._surf_display.blit(self.imgMask, (180,60))
        pygame.display.flip()
        pygame.time.wait(1000)
        self._surf_display.fill(NEUTRAL_BLUE)
        pygame.display.flip()

    def showText(self, textInput):
        textDisp = smallFont.render(textInput[:50], True, BLACK, NEUTRAL_BLUE)
        textRect = textDisp.get_rect()
        textRect.left = 0
        textRect.top = 330
        self._surf_display.blit(textDisp, textRect)
        if len(textInput) > 50:
            textDisp2 = smallFont.render(textInput[50:100], True, BLACK, NEUTRAL_BLUE)
            textRect2 = textDisp2.get_rect()
            textRect2.left = 100
            textRect2.top = 360
            self._surf_display.blit(textDisp2, textRect2)
        if len(textInput) > 100:
            textDisp3 = smallFont.render(textInput[100:150], True, BLACK, NEUTRAL_BLUE)
            textRect3 = textDisp3.get_rect()
            textRect3.left = 100
            textRect3.top = 390
            self._surf_display.blit(textDisp3, textRect3)
        if len(textInput) > 150:
            textDisp4 = smallFont.render(textInput[150:200], True, BLACK, NEUTRAL_BLUE)
            textRect4 = textDisp4.get_rect()
            textRect4.left = 100
            textRect4.top = 420
            self._surf_display.blit(textDisp4, textRect4)
                
        #pygame.display.flip()

    def showQuestion(self, prompt, maxLen):#maxLen is the maximum length of a single line of text before a carriage return
        if not (prompt == None):
            if (len(prompt) < maxLen):
                textDisp = smallFont.render(prompt, True, BLACK, TRANSPARENT)
                textDisp.set_colorkey(TRANSPARENT)
                textRect = textDisp.get_rect()
                textRect.left = 512 - (textDisp.get_width() / 2)
                textRect.top = 250

                self._surf_display.blit(textDisp, textRect)

            else:
                tempText1 = prompt[:(maxLen + 1)]
                tempText2 = prompt[(maxLen + 1):]

                textDisp1 = smallFont.render(tempText1, True, BLACK, TRANSPARENT)
                textDisp1.set_colorkey(TRANSPARENT)
                textRect1 = textDisp1.get_rect()
                textRect1.left = 512 - (textDisp1.get_width() / 2)
                textRect1.top = 250

                textDisp2 = smallFont.render(tempText2, True, BLACK, TRANSPARENT)
                textDisp2.set_colorkey(TRANSPARENT)
                textRect2 = textDisp2.get_rect()
                textRect2.centerx = 512 - (textDisp2.get_width() / 2)
                textRect2.top = 290
                
                self._surf_display.blit(textDisp1, textRect1)
                self._surf_display.blit(textDisp2, textRect2)
                
            pygame.display.flip()
        else:
            pass
        
    def clearScreen(self):
        self._surf_display.blit(self.background, (0,0))
        pygame.display.flip()

    def getQuestions(self):
##        dispNum = 1
##        while (dispNum < QUESTION_COUNT):            
        for row in self.f:
            for col in row:
                self.questionList.append(col)
                #dispNum += 1
        self.questionMax = len(self.questionList)
        i = 0
        for q in self.questionList:
            self.questionDict[i] = q
            #print self.questionDict[i]
            i += 1

        #for n in self.questionDict.values(): #DEBUG
            #print n

        dispNum = 0
        while (dispNum < len(self.questionDict)):    
            randrow = random.randint(0,len(self.questionDict)-1)
            rownum = 0
            for row in self.questionDict.keys():
                if (rownum == randrow):
                    if (self.keyList.count(row) == 0):
                        print row
                        self.newList.append(self.questionDict[row])
                        self.keyList.append(row)
                        dispNum += 1
                rownum += 1

        for i in range(len(self.newList)): #DEBUG
            print self.newList[i]
               
    def eventmanager(self, event):
        if event.type == pygame.QUIT:
            self.isRunning = False
        #if event.type == timeOut:
        #    self.isRunning = False
        #check for keypresses    
        elif event.type == KEYDOWN:
            #if the player presses Esc, quit
            if event.key == K_ESCAPE:
                self.isRunning = False
                #sys.exit()
            if event.key == K_RETURN or event.key == K_KP_ENTER:
                #self.trialTimer = 0
                self._surf_display.blit(self.background, (0,0))
                self.responseLog.append(self.inputBuffer)
                self.inputBuffer = str()
                if self.questionNum < self.questionMax:
                    self.questionNum += 1
                self.showMask()
            if event.key == K_BACKSPACE:
                if not(self.inputBuffer == ''):
                    self._surf_display.blit(self.background, (0,0))
                    self.inputBuffer = self.inputBuffer[:(len(self.inputBuffer) - 1)]
            if len(self.inputBuffer) < 200:    
                if event.mod & KMOD_SHIFT:
                    if event.key == K_MINUS:
                        self.inputBuffer = self.inputBuffer + '_'
                    if event.key == K_SLASH:
                        self.inputBuffer = self.inputBuffer + '?'
                    if event.key == K_COMMA:
                        self.inputBuffer = self.inputBuffer + '<'
                    if event.key == K_PERIOD:
                        self.inputBuffer = self.inputBuffer + '>'
                    if event.key == K_QUOTE:
                        self.inputBuffer = self.inputBuffer + '\"'
                    if event.key == K_EQUALS:
                        self.inputBuffer = self.inputBuffer + '+'
                    if event.key == K_SEMICOLON:
                        self.inputBuffer = self.inputBuffer + ':'
                    if event.key == K_BACKSLASH:
                        self.inputBuffer = self.inputBuffer + '|'
                    if event.key == K_LEFTBRACKET:
                        self.inputBuffer = self.inputBuffer + '{'
                    if event.key == K_RIGHTBRACKET:
                        self.inputBuffer = self.inputBuffer + '}'
                    if event.key == K_BACKQUOTE:
                        self.inputBuffer = self.inputBuffer + '~'
                    if event.key == K_1:
                        self.inputBuffer = self.inputBuffer + '!'
                    if event.key == K_2:
                        self.inputBuffer = self.inputBuffer + '@'
                    if event.key == K_3:
                        self.inputBuffer = self.inputBuffer + '#'
                    if event.key == K_4:
                        self.inputBuffer = self.inputBuffer + '$'
                    if event.key == K_6:
                        self.inputBuffer = self.inputBuffer + '^'
                    if event.key == K_7:
                        self.inputBuffer = self.inputBuffer + '&'
                    if event.key == K_8:
                        self.inputBuffer = self.inputBuffer + '*'
                    if event.key == K_9:
                        self.inputBuffer = self.inputBuffer + '('
                    if event.key == K_0:
                        self.inputBuffer = self.inputBuffer + ')'
                    if event.key == K_a:
                        self.inputBuffer = self.inputBuffer + 'A'
                    if event.key == K_b:
                        self.inputBuffer = self.inputBuffer + 'B'
                    if event.key == K_c:
                        self.inputBuffer = self.inputBuffer + 'C'
                    if event.key == K_d:
                        self.inputBuffer = self.inputBuffer + 'D'
                    if event.key == K_e:
                        self.inputBuffer = self.inputBuffer + 'E'
                    if event.key == K_f:
                        self.inputBuffer = self.inputBuffer + 'F'
                    if event.key == K_g:
                        self.inputBuffer = self.inputBuffer + 'G'
                    if event.key == K_h:
                        self.inputBuffer = self.inputBuffer + 'H'
                    if event.key == K_i:
                        self.inputBuffer = self.inputBuffer + 'I'
                    if event.key == K_j:
                        self.inputBuffer = self.inputBuffer + 'J'
                    if event.key == K_k:
                        self.inputBuffer = self.inputBuffer + 'K'
                    if event.key == K_l:
                        self.inputBuffer = self.inputBuffer + 'L'
                    if event.key == K_m:
                        self.inputBuffer = self.inputBuffer + 'M'
                    if event.key == K_n:
                        self.inputBuffer = self.inputBuffer + 'N'
                    if event.key == K_o:
                        self.inputBuffer = self.inputBuffer + 'O'
                    if event.key == K_p:
                        self.inputBuffer = self.inputBuffer + 'P'
                    if event.key == K_q:
                        self.inputBuffer = self.inputBuffer + 'Q'
                    if event.key == K_r:
                        self.inputBuffer = self.inputBuffer + 'R'
                    if event.key == K_s:
                        self.inputBuffer = self.inputBuffer + 'S'
                    if event.key == K_t:
                        self.inputBuffer = self.inputBuffer + 'T'
                    if event.key == K_u:
                        self.inputBuffer = self.inputBuffer + 'U'
                    if event.key == K_v:
                        self.inputBuffer = self.inputBuffer + 'V'
                    if event.key == K_w:
                        self.inputBuffer = self.inputBuffer + 'W'
                    if event.key == K_x:
                        self.inputBuffer = self.inputBuffer + 'X'
                    if event.key == K_y:
                        self.inputBuffer = self.inputBuffer + 'Y'
                    if event.key == K_z:
                        self.inputBuffer = self.inputBuffer + 'Z'
                else:
                    if event.key == K_a:
                        self.inputBuffer = self.inputBuffer + 'a'
                    if event.key == K_b:
                        self.inputBuffer = self.inputBuffer + 'b'
                    if event.key == K_c:
                        self.inputBuffer = self.inputBuffer + 'c'
                    if event.key == K_d:
                        self.inputBuffer = self.inputBuffer + 'd'
                    if event.key == K_e:
                        self.inputBuffer = self.inputBuffer + 'e'
                    if event.key == K_f:
                        self.inputBuffer = self.inputBuffer + 'f'
                    if event.key == K_g:
                        self.inputBuffer = self.inputBuffer + 'g'
                    if event.key == K_h:
                        self.inputBuffer = self.inputBuffer + 'h'
                    if event.key == K_i:
                        self.inputBuffer = self.inputBuffer + 'i'
                    if event.key == K_j:
                        self.inputBuffer = self.inputBuffer + 'j'
                    if event.key == K_k:
                        self.inputBuffer = self.inputBuffer + 'k'
                    if event.key == K_l:
                        self.inputBuffer = self.inputBuffer + 'l'
                    if event.key == K_m:
                        self.inputBuffer = self.inputBuffer + 'm'
                    if event.key == K_n:
                        self.inputBuffer = self.inputBuffer + 'n'
                    if event.key == K_o:
                        self.inputBuffer = self.inputBuffer + 'o'
                    if event.key == K_p:
                        self.inputBuffer = self.inputBuffer + 'p'
                    if event.key == K_q:
                        self.inputBuffer = self.inputBuffer + 'q'
                    if event.key == K_r:
                        self.inputBuffer = self.inputBuffer + 'r'
                    if event.key == K_s:
                        self.inputBuffer = self.inputBuffer + 's'
                    if event.key == K_t:
                        self.inputBuffer = self.inputBuffer + 't'
                    if event.key == K_u:
                        self.inputBuffer = self.inputBuffer + 'u'
                    if event.key == K_v:
                        self.inputBuffer = self.inputBuffer + 'v'
                    if event.key == K_w:
                        self.inputBuffer = self.inputBuffer + 'w'
                    if event.key == K_x:
                        self.inputBuffer = self.inputBuffer + 'x'
                    if event.key == K_y:
                        self.inputBuffer = self.inputBuffer + 'y'
                    if event.key == K_z:
                        self.inputBuffer = self.inputBuffer + 'z'
                    if event.key == K_0:
                        self.inputBuffer = self.inputBuffer + '0'
                    if event.key == K_1:
                        self.inputBuffer = self.inputBuffer + '1'
                    if event.key == K_2:
                        self.inputBuffer = self.inputBuffer + '2'
                    if event.key == K_3:
                        self.inputBuffer = self.inputBuffer + '3'
                    if event.key == K_4:
                        self.inputBuffer = self.inputBuffer + '4'
                    if event.key == K_5:
                        self.inputBuffer = self.inputBuffer + '5'
                    if event.key == K_6:
                        self.inputBuffer = self.inputBuffer + '6'
                    if event.key == K_7:
                        self.inputBuffer = self.inputBuffer + '7'
                    if event.key == K_8:
                        self.inputBuffer = self.inputBuffer + '8'
                    if event.key == K_9:
                        self.inputBuffer = self.inputBuffer + '9'
                    if event.key == K_PERIOD or event.key == K_KP_PERIOD:
                        self.inputBuffer = self.inputBuffer + '.'
                    if event.key == K_COMMA:
                        self.inputBuffer = self.inputBuffer + ','
                    if event.key == K_QUOTE:
                        self.inputBuffer = self.inputBuffer + '\''
                    if event.key == K_KP0:
                        self.inputBuffer = self.inputBuffer + '0'
                    if event.key == K_KP1:
                        self.inputBuffer = self.inputBuffer + '1'
                    if event.key == K_KP2:
                        self.inputBuffer = self.inputBuffer + '2'
                    if event.key == K_KP3:
                        self.inputBuffer = self.inputBuffer + '3'
                    if event.key == K_KP4:
                        self.inputBuffer = self.inputBuffer + '4'
                    if event.key == K_KP5:
                        self.inputBuffer = self.inputBuffer + '5'
                    if event.key == K_KP6:
                        self.inputBuffer = self.inputBuffer + '6'
                    if event.key == K_KP7:
                        self.inputBuffer = self.inputBuffer + '7'
                    if event.key == K_KP8:
                        self.inputBuffer = self.inputBuffer + '8'
                    if event.key == K_KP9:
                        self.inputBuffer = self.inputBuffer + '9'

                    if event.key == K_SPACE:
                        self.inputBuffer = self.inputBuffer + ' '                
                    if event.key == K_KP_MULTIPLY:
                        self.inputBuffer = self.inputBuffer + '*'
                    if event.key == K_KP_PLUS:
                        self.inputBuffer = self.inputBuffer + '+'
                    if event.key == K_MINUS or event.key == K_KP_MINUS:
                        self.inputBuffer = self.inputBuffer + '-'
                    if event.key == K_EQUALS or event.key == K_KP_EQUALS:
                        self.inputBuffer = self.inputBuffer + '='
                    if event.key == K_SLASH or event.key == K_KP_DIVIDE:
                        self.inputBuffer = self.inputBuffer + '/'
                    if event.key == K_SEMICOLON:
                        self.inputBuffer = self.inputBuffer + ';'
                    if event.key == K_LEFTBRACKET:
                        self.inputBuffer = self.inputBuffer + '['
                    if event.key == K_RIGHTBRACKET:
                        self.inputBuffer = self.inputBuffer + ']'
                    if event.key == K_BACKSLASH:
                        self.inputBuffer = self.inputBuffer + '\\'
                    if event.key == K_BACKQUOTE:
                        self.inputBuffer = self.inputBuffer + '`'

    def on_execute(self, FPS):
        #set up clock
        clock = pygame.time.Clock()
        FRAMES_PER_SECOND = FPS
        #countDown = 450#TEMP

        while(self.isRunning):
            #advance the clock 1 tick
            clock.tick(FRAMES_PER_SECOND)
            #self.trialTimer += 1
            #DEBUG print self.trialTimer
            #DEBUG print self.trialMaxTime * FRAMERATE
##            if self.trialTimer == self.trialMaxTime * FRAMERATE:
##                self.trialTimer = 0
##                self._surf_display.blit(self.background, (0,0))
##                self.responseLog.append(self.inputBuffer)
##                self.showMask()
##                self.inputBuffer = str()
##                if self.questionNum < self.questionMax:
##                    self.questionNum += 1
            for event in pygame.event.get():
                self.eventmanager(event)
            if self.questionNum == self.questionMax:
                self.isRunning = False
            else:
                #DEBUG print self.questionNum
                self.showQuestion(self.newList[self.questionNum], 100)
                #self.showQuestion(self.questionDict[self.questionNum], 100)
                self.showText(self.inputBuffer)
            #countDown = countDown - 1 #TEMP
            #if countDown == 0:
            #    self.isRunning = False
            pygame.display.flip()

if __name__=="__main__":

    gsForm = experiment()

    gsForm.showMask() #this will display a random pattern of white and black pixels to remove after-image
    gsForm.clearScreen()
    pygame.event.clear()

    gsForm.getQuestions()

    #for i in gsForm.questionList: #DEBUG
    #    print i

    gsForm.on_execute(FRAMERATE)   

    #Close data file and quit
    #dataFilename.close()

    #reverse the response log lists so that list will be in the same order as presented
    gsForm.responseLog.reverse()
    gsForm.keyList.reverse()

    #copy the lists of user feedback info into a CSV file.

    dataFilename = open('formdata.csv', 'wb')
    dataFile = csv.writer(dataFilename, delimiter=',', quotechar='|')

    while (len(gsForm.responseLog) > 0):
        dataFile.writerow([gsForm.keyList.pop(),gsForm.responseLog.pop()])
        #DEBUG print gsForm.responseLog.pop()

    dataFilename.close()

    pygame.quit()

##SimpleCamVC
##
##Simple camera capture program heavily dependent on the VideoCapture 0.9.3 library
##
##This program captures and saves a specified number of still shots at a specified rate.
##That is all. It was developed for a very specific application.
##It could be very easily modified to do stop-motion animation - just change the
##frames per second to a ridiculously small number.
##
##Currently there are no dialogs - change the FRAMES_PER_SECOND variable to whatever
##number you like. Same goes for numFrames. You can do the math. If you prefer
##to use dialogs, just delete the numbers and uncomment the "input()"s

from VideoCapture import *
import pygame, sys, os, cv
from pygame.locals import *

#initialize pygame
pygame.init()

READY = (160,160,160)
BLACK = (0,0,0)
screenSize = (1366,768)
_surf_display = None
_surf_display = pygame.display.set_mode(screenSize, FULLSCREEN)

#initialize camera
cam = Device()

mainSurf = pygame.Surface(screenSize)
mainSurf.fill(READY)

_surf_display.blit(mainSurf, (0,0))
pygame.display.update()

#frames per second
FRAMES_PER_SECOND = 2#input('How many frames per second?')
#number of frames to capture
numFrames = 240#input('How many frames to capture?')

#set up clock
clock = pygame.time.Clock()

def run_loop(FPS,frameMax):
    i=1
    while (i==1):
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    i=0
                    mainSurf.fill(BLACK)
                    _surf_display.blit(mainSurf, (0,0))
                    pygame.display.update()
                if event.key == K_ESCAPE:
                    pygame.quit()
    j=0
    while (j<frameMax):
        clock.tick(FPS)
        fileName = 'image' + str(j) + '.jpg'
        cam.saveSnapshot(fileName, timestamp=0)    
        j = j + 1
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()

run_loop(FRAMES_PER_SECOND,numFrames)

pygame.quit()

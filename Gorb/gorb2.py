#A special-purpose modification of my random digit series generator

import random

def gorb():
    print('This version of Gorb will only generate numbers starting with 2 or higher, e.g., 24, 340, 2, 93465.')
    filename = raw_input('Enter the name of your savefile:')
    f = open(filename + '.txt', 'w')
    numStrings = int(raw_input('Enter the number of strings to generate:'))
    numDigits = int(raw_input('Enter the number of digits in each string:'))
    gorbString = ''
    for g in range(numStrings - 1):
        for i in range(numDigits):
            if (i == 0):
                gorbString = gorbString + str(random.randint(2,9))
            elif (i > 0):
                gorbString = gorbString + str(random.randint(0,9))
        gorbString = gorbString + '\n'
    for i in range(numDigits):
        if (i == 0):
            gorbString = gorbString + str(random.randint(2,9))
        elif (i > 0):
            gorbString = gorbString + str(random.randint(0,9))
    f.write(gorbString)

gorb()



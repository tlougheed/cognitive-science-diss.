#Facepaste 0.6 by Ted Lougheed - automatic pasting utility
#Last updated: 7-May-2012
#
#This program is designed for a very specific use in a cognitive psychology
#experiment. It pastes images at specific locations in target files (using the
#pygame libraries), specified in a CSV file that the user can edit.
#The program is intended to superimpose that image over the image of the
#protagonist in a series of scenario images. It can easily be repurposed
#to do batch pasting jobs of any variety.

import pygame, math, sys, csv
#import numpy, scipy, cv
#sys.path.append("C:\OpenCV2.2\Python2.7\Lib\site-packages")
#import cv
from pygame.locals import *
#from VideoCapture import *

#initialize pygame module
pygame.init()

#define main function
def main():
    
    #define cursor position
    cursorPos = 0,0

    #set up colors
    TRANSPARENT = (0,0,1)
    BLACK = (0,0,0)
    WHITE = (255,255,255)
    RED = (255,0,0)
    GREEN = (0,255,0)
    BLUE = (0,0,255)
    DARKBLUE = (0,0,127)
    LIGHTBLUE = (127,127,255)

    #set up window
    #set window caption
    pygame.display.set_caption('Facepaste 0.6')
    #load and set logo
    #create the main surface on the screen
    screenWidth = 800
    screenHeight = 600
    screen = pygame.display.set_mode((screenWidth, screenHeight), DOUBLEBUF)
    pygame.display.flip()
  
    #set up fonts
    titleFont = pygame.font.SysFont(None, 16)
    basicFont = pygame.font.SysFont(None, 12)

    #set up clock
    clock = pygame.time.Clock()
    FRAMES_PER_SECOND = 30

    #set up text (temp)
    text = titleFont.render('Facepaste 0.6', True, WHITE, BLACK)
    textRect = text.get_rect()
    textRect.left = 16
    textRect.top = 16

    #draw background
    screen.fill(BLACK)
    #OLD worldSurf.fill(BLACK)
    #OLD screen.blit(worldSurf,(((worldWidth/2)-(screenWidth/2)),((worldHeight/2)-(screenHeight/2))))
            
    #define a variable to control the main loop
    running = True
    
    pygame.display.flip()

    #print('Press SPACE to begin.')

#load csv file containing pasting positions for each scenario file
    csvFilename = raw_input('Enter the name of the index file (CSV format): ')
    posReader = csv.reader(open(csvFilename, 'rb'), delimiter=',', quotechar='|')

    #load participant image
    inputImgNum = raw_input('Enter the participant #: ')
    inputImg = pygame.image.load('facegrab' + inputImgNum + '.png')
    inputImg.set_colorkey(GREEN)
    resizedImg = pygame.transform.scale(inputImg, (90, 100))
    inputImgRect = inputImg.get_rect()
    resizedImgRect = resizedImg.get_rect()

    #iterate through the list of files and positions, pasting the input image onto the background image in the right location
    rownum = 0
    for row in posReader:
        colnum = 0
        if rownum > 0:
            for col in row:
                if colnum == 0:
                    outputFilename = col
                    backdrop = pygame.image.load(col)
                elif colnum == 1:
                    xpos = int(col)
                elif colnum == 2:
                    ypos = int(col)
                colnum +=1
            ##DEBUG print '{}, {}'.format(xpos, ypos)
            #display backdrop image
            screen.blit(backdrop, (0,0))
            #display input image (centered on positions defined in csv file) on top of background image
            if (rownum > 6) and (rownum < 10): 
                screen.blit(inputImg, ((xpos - inputImgRect.centerx),(ypos - inputImgRect.centery)))
            else:
                screen.blit(resizedImg, ((xpos - resizedImgRect.centerx),(ypos - resizedImgRect.centery)))
            #flip the display
            pygame.display.flip()
            #save the new image over the old backdrop image
            pygame.image.save(screen, outputFilename)            
        rownum += 1

    screen.fill(BLUE)
    pygame.display.flip()

    #main loop
    while running:
        #advance the clock 1 tick
        clock.tick(FRAMES_PER_SECOND)

        ##TEMP pygame.camera.list_cameras()

        #USER_INPUT
        #event handling, gets all events from the eventqueue
        for event in pygame.event.get():
            #if the user clicks the X, quit
            if event.type == QUIT:
                #change value to false, exit main loop
                running = False
              #  sys.exit()
            if not hasattr(event, 'key'): continue
            #exit loop if the player presses Esc
            if (event.key == K_ESCAPE):
                running = False
            #paste face onto scenario backdrops
            if (event.type == KEYUP):
                if (event.key == K_SPACE):
                    print('Spaaaaaaaaaaaace')
##                    #load csv file containing pasting positions for each scenario file
##                    csvFilename = raw_input('Enter the name of the index file (CSV format): ')
##                    posReader = csv.reader(open(csvFilename, 'rb'), delimiter=',', quotechar='|')
##
##                    #load participant image
##                    inputImgNum = raw_input('Enter the participant #: ')
##                    inputImg = pygame.image.load('facegrab' + inputImgNum + '.png')
##                    inputImg.set_colorkey(GREEN)
##                    inputImgRect = inputImg.get_rect()
##
##                    #iterate through the list of files and positions, pasting the input image onto the background image in the right location
##                    rownum = 0
##                    for row in posReader:
##                        colnum = 0
##                        if rownum > 0:
##                            for col in row:
##                                if colnum == 0:
##                                    outputFilename = col
##                                    backdrop = pygame.image.load(col)
##                                elif colnum == 1:
##                                    xpos = int(col)
##                                elif colnum == 2:
##                                    ypos = int(col)
##                                colnum +=1
##                            ##DEBUG print '{}, {}'.format(xpos, ypos)
##                            #display backdrop image
##                            screen.blit(backdrop, (0,0))
##                            #display input image (centered on positions defined in csv file) on top of background image
##                            screen.blit(inputImg, ((xpos - inputImgRect.centerx),(ypos - inputImgRect.centery)))
##                            #flip the display
##                            pygame.display.flip()
##                            #save the new image over the old backdrop image
##                            pygame.image.save(screen, outputFilename)            
##                        rownum += 1

    #If the running flag is false, quit the program.
    if running == False:
        pygame.quit()

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":
    #call main function
    main()

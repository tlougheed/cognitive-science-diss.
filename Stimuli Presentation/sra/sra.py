##STIMULI:
##
## 60 randomly-generated adjectives and a question about how well they describe a person
##
##PROCEDURE:
##
##Display random word from set of 60 adjectives.
##when player presses a number key, move to the next word.
##repeat until list is exhausted.

import pygame, math, sys, csv, random
from pygame.locals import *

pygame.init()

pygame.display.set_caption('_')

TRANSPARENT = (0,0,1)
BLACK = (0,0,0)
WHITE = (255,255,255)
RED = (255,0,0)
GREEN = (0,128,0)
BLUE = (0,0,255)
BROWN = (150,75,0)

_surf_display = None
_surf_display = pygame.display.set_mode((800,600), FULLSCREEN)
_surf_display.fill(BLACK)

background = pygame.Surface((800,600))
background.set_colorkey(TRANSPARENT)
background.fill(BLACK)

basicFont = pygame.font.SysFont(None, 72)
smallFont = pygame.font.SysFont(None, 36)

#list to hold data (keyboard inputs)
kLog = list()
subjLog = list()
wordLog = list()

f = csv.reader(open('wordset.txt', 'rb'), delimiter=',', quotechar='|')

usedList1 = list()

def showMask():
    imgMask = pygame.image.load('whitenoise.jpg')
    _surf_display.blit(imgMask, (80,60))
    pygame.display.flip()
    pygame.time.wait(500)

def showText(textInput):

    textDisp = basicFont.render(textInput, True, WHITE, TRANSPARENT)
    textDisp.set_colorkey(TRANSPARENT)
    textRect = textDisp.get_rect()
    textRect.left = 400 - (textDisp.get_width() / 2)
    textRect.top = 250
    _surf_display.blit(textDisp, textRect)
    pygame.display.flip()

def getInput(prompt, maxLen):#maxLen is the maximum length of a single line of text before a carriage return
    pygame.time.set_timer(USEREVENT+1, 10000)
    if not (prompt == None):
        if (len(prompt) < maxLen):
            textDisp = smallFont.render(prompt, True, WHITE, TRANSPARENT)
            textDisp.set_colorkey(TRANSPARENT)
            textRect = textDisp.get_rect()
            textRect.left = 400 - (textDisp.get_width() / 2)
            textRect.top = 350

            _surf_display.blit(textDisp, textRect)

        else:
            tempText1 = prompt[:(maxLen + 1)]
            tempText2 = prompt[(maxLen + 1):]

            textDisp1 = smallFont.render(tempText1, True, WHITE, TRANSPARENT)
            textDisp1.set_colorkey(TRANSPARENT)
            textRect1 = textDisp1.get_rect()
            textRect1.left = 400 - (textDisp1.get_width() / 2)
            textRect1.top = 350

            textDisp2 = smallFont.render(tempText2, True, WHITE, TRANSPARENT)
            textDisp2.set_colorkey(TRANSPARENT)
            textRect2 = textDisp2.get_rect()
            textRect2.left = 400 - (textDisp2.get_width() / 2)
            textRect2.top = 390
            
            _surf_display.blit(textDisp1, textRect1)
            _surf_display.blit(textDisp2, textRect2)
            
        pygame.display.flip()
    else:
        pass
    
    waitForEvent = 1
    while (waitForEvent == 1):
        for event in pygame.event.get():
            if event.type == USEREVENT+1:
                kLog.append('NA')
                waitForEvent = 0
            if event.type == KEYDOWN:
                if event.key == K_1:#descriptiveness rating 1 (lowest)
                    kLog.append('1')
                    waitForEvent = 0
                if event.key == K_2:#descriptiveness rating 2
                    kLog.append('2')
                    waitForEvent = 0
                if event.key == K_3:#descriptiveness rating 3
                    kLog.append('3')
                    waitForEvent = 0
                if event.key == K_4:#descriptiveness rating 4
                    kLog.append('4')
                    waitForEvent = 0

def clearScreen():
    _surf_display.blit(background, (0,0))
    pygame.display.flip()

def runTrials():

    dispNum = 1

    while (dispNum < 61):
        f1 = csv.reader(open('wordset.txt', 'rb'), delimiter=',', quotechar='|')    
        randrow = random.randint(1,60)
        #DEBUG print(randrow)
        rownum = 1
        for row in f1:
            for col in row:
                if (rownum == randrow):
                    if (usedList1.count(col) == 0):
                        #DEBUG print(col)
                        showMask()
                        clearScreen()
                        showText(col)
                        randInput = random.randint(0,1)
                        if randInput == 0:
                            getInput('How well does this word describe you?',50)
                            wordLog.append(col)
                            subjLog.append('0')
                        else:
                            getInput('How well does this word describe Stephen Harper?',50)
                            wordLog.append(col)
                            subjLog.append('1')
                        clearScreen()
                        #display word
                        #add word to used list so it does not appear again
                        #DEBUG print(col)
                        usedList1.append(col)
                        dispNum += 1
            rownum += 1

    #empty the used list so that we can run though the set of words again
    #this isn't necessary, but I left it in just in case.
    while len(usedList1) > 0 : usedList1.pop()

##run trials
runTrials()

showMask() #this will display a random pattern of white and black pixels to remove after-image
clearScreen()

#reverse the key log and word log lists so that lists will be in the same order as presented
kLog.reverse()
subjLog.reverse()
wordLog.reverse()

#copy the lists of user feedback info into a CSV file.

dataFilename = open('sraDataLog.csv', 'wb')
dataFile = csv.writer(dataFilename, delimiter=',', quotechar='|')

i = 1
while (len(wordLog) > 0):
    dataFile.writerow([subjLog.pop(),wordLog.pop(),kLog.pop()])
    i += 1

dataFilename.close()

#quit the program
pygame.quit()

import pygame, math, sys
from pygame.locals import *

#define main function
def main():
    
    #define cursor position
    cursorPos = 0,0

    #set up colors
    BLACK = (0,0,0)
    WHITE = (255,255,255)
    RED = (255,0,0)
    GREEN = (0,255,0)
    BLUE = (0,0,255)
    DARKBLUE = (0,0,127)
    LIGHTBLUE = (127,127,255)

    #initialize pygame module
    pygame.init()

    #set up window
    #set window caption
    pygame.display.set_caption('Cam Capture Demo')
    #load and set logo
    #create the main surface on the screen
    screenWidth = 800
    screenHeight = 600
    screen = pygame.display.set_mode((screenWidth, screenHeight), DOUBLEBUF)
    pygame.display.flip()
  
    #set up fonts
    titleFont = pygame.font.SysFont(None, 16)
    basicFont = pygame.font.SysFont(None, 12)

    #set up clock
    clock = pygame.time.Clock()
    FRAMES_PER_SECOND = 30

    #set up text (temp)
    text = titleFont.render('Cam Capture Demo', True, WHITE, BLUE)
    textRect = text.get_rect()
    textRect.left = 0
    textRect.top = 0

    #set up world surface
    worldWidth = 800
    worldHeight = 600
    worldSurf = pygame.Surface((worldWidth,worldHeight))
    #i'll have to change this later, since where the worldSurf
    #appears will depend on where the player spawns.
    #Once multiplayer is in place, it will be given by the server
    
    #draw background
    screen.fill(BLUE)
    worldSurf.fill(BLACK)
    screen.blit(worldSurf,(((worldWidth/2)-(screenWidth/2)),((worldHeight/2)-(screenHeight/2))))#,((screenWidth/2),(screenHeight/2)))

    #display title on the screen
    #DISABLED screen.blit(text, textRect)
    
    
    #define a variable to control the main loop
    running = True

#class Controller:
#    def Notify(self, event):


    #main loop
    while running:
        #advance the clock 1 tick
        clock.tick(FRAMES_PER_SECOND)

        ##TEMP##pygame.camera.list_cameras()

        #USER_INPUT
        #event handling, gets all events from the eventqueue
        for event in pygame.event.get():
            #if the user clicks the X, quit
            if event.type == QUIT:
                #change value to false, exit main loop
                running = False
              #  sys.exit()
            if event.type == MOUSEMOTION:
                cursorPos = event.pos
            if not hasattr(event, 'key'): continue
            #exit loop if the player presses Esc
            if (event.key == K_ESCAPE):
                running = False
    
        # SIMULATION                

        # RENDERING
        
        # Wipe the screen (later I'll just update the dirty_rects)
        screen.fill(BLUE)
        worldSurf.fill(BLACK)

        #render world to screen
        screen.blit(worldSurf, (0,0))#, ((screenWidth/2),(screenHeight/2)))
        pygame.display.flip()

    if running == False:
        pygame.quit()

#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":
    #call main function
    main()

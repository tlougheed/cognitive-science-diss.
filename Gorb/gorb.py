#simple program for generating random series of digits of any length

import random

def gorb():
    filename = raw_input('Enter the name of your savefile:')
    f = open(filename + '.txt', 'w')
    numStrings = int(raw_input('Enter the number of strings to generate:'))
    numDigits = int(raw_input('Enter the number of digits in each string:'))
    gorbString = ''
    for g in range(numStrings - 1):
        for i in range(numDigits):
            gorbString = gorbString + str(random.randint(0,9))
        gorbString = gorbString + '\n'
    for i in range(numDigits):
        gorbString = gorbString + str(random.randint(0,9))
    f.write(gorbString)

gorb()



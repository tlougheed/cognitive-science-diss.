##Tetris/Temporal-Causal Sequence Task
##Ted Lougheed
##22-Oct-2012
##
##Tetris game is a modified version of Bobby Lee's "Tetris" game,
##available at www.pygame.org, which is a clone of the game originally
##developed by Alexey Pajitnov. It has been modified to be suitable for
##the purposes of this experiment. Since it is based on copyrighted material,
##it is not to be distributed for profit and will only be used for the
##purposes of this experiment.
##
##In this task, the participant is presented with a game of Tetris
##and an audio recording of a short narrative, either featuring a
##self- or other-related pronoun.
##The participant is asked to try to get the highest score she can.
##She will be informed that losing the game will reset the score and the game,
##but that play will continue. She will be informed that only the score
##on the last game will counted. The program will record all scores and
##the number of times that the game restarted.
##
##The participant is given a practice round to get acclimated to playing Tetris
##(this could be adjusted depending on familiarity with Tetris).
##
##During the task following the practice round, the participant will get
##a head-start on the Tetris game. The audio recording will start
##30 seconds to a minute into the game. The participant will be told this
##in advance.
##
##

import pygame, math, random, csv, sys, os, time
from pygame.locals import *

#define color words
TRANSPARENT = (0,0,1) #set aside almost-black for use as colorkey
BLACK = (0,0,0)
WHITE = (255,255,255)
SILVER = (192,192,192)
GRAY = (128,128,128)
RED = (255,0,0)
MAROON = (128,0,0)
YELLOW = (255,255,0)
OLIVE = (128,128,0)
LIME = (0,255,0)
GREEN = (0,128,0)
AQUA = (0,255,255)
TEAL = (0,128,128)
BLUE = (0,0,255)
NAVY = (0,0,128)
FUCHSIA = (255,0,255)
PURPLE = (128,0,128)
SUNSET = (255,128,128)
ORANGE = (255,128,0)
LIGHTGREEN = (128, 255, 128)
UN_BLUE = (128,128,255)
AZURE = (0,128,255)

#Define other constants
WAITING_TIME=0
FPS=3
START_COORDINATES=(180,50)
BLOCK_WIDTH=24
SCREEN_SIZE=[1366,768]
TETRA_CHOICES=(1,4)

#When does the audio start? (in ms)
TIME_START_AUDIO= 20000

#audio file is at least 257000 ms long
##Game length, in milliseconds
GAME_LENGTH = 300000
PRACTICE_LENGTH = 60000

GRID_SIZE_X=12
GRID_SIZE_Y=24

#create list to store scores
Scores = list()

#initiate pygame
pygame.init()

pygame.mouse.set_visible(False)

#fonts
basicFont = pygame.font.SysFont(None, 72)

#SOUND
pygame.mixer.init()

#TESTING ONLY:
AudioNarrative = pygame.mixer.Sound('you_narrative.wav') #Just a dummy file right now. Will replace it with the real file once it is recorded

#TETRIS CODE
class Block(pygame.sprite.Sprite):
    def __init__ (self,block_type, xy):
        "just the basic building block of tetras, makes up the grid"
        pygame.sprite.Sprite.__init__(self)
        self.block_type=block_type
        if block_type==1:            
            self.image =pygame.image.load( os.path.join( 'basic_block1.gif' ))
        elif block_type==2:
            self.image =pygame.image.load( os.path.join( 'basic_block2.gif' ))
        elif block_type==3:    
            self.image =pygame.image.load( os.path.join( 'basic_block3.gif' ))
        elif block_type==4:
            self.image =pygame.image.load( os.path.join( 'basic_block4.gif' ))
        elif block_type==5:
            self.image =pygame.image.load( os.path.join( 'basic_block5.gif' ))
        self.image.convert()
        self.rect = self.image.get_rect()
        
        self.rect.left, self.rect.top = xy
    def transform(self,block_type):
        self.block_type=block_type
        if block_type==1:            
            self.image =pygame.image.load( os.path.join( 'basic_block1.gif' ))
        elif block_type==2:
            self.image =pygame.image.load( os.path.join( 'basic_block2.gif' ))
        elif block_type==3:    
            self.image =pygame.image.load( os.path.join( 'basic_block3.gif' ))
        elif block_type==4:
            self.image =pygame.image.load( os.path.join( 'basic_block4.gif' ))
        elif block_type==5:
            self.image =pygame.image.load( os.path.join( 'basic_block5.gif' ))
        self.image.convert()
            
class Game(object):
    def __init__(self):
        pygame.init()     
        self.window = pygame.display.set_mode(SCREEN_SIZE,FULLSCREEN)
        
        self.clock = pygame.time.Clock()
        
        self.background = pygame.Surface(SCREEN_SIZE)
        self.background.fill(GRAY)
          
        fonts = pygame.font.get_fonts()

        #self.font1 = pygame.font.Font(pygame.font.match_font("Arial"), 32)
        self.font1 = pygame.font.SysFont(None, 32)
        self.score=0
        self.text1 = self.font1.render('Score: %s'%self.score, 1, BLACK)#,(159, 182,205))
        
        self.rect1 = self.text1.get_rect()
        
        self.rect1.top = 150
        self.rect1.left = 830

        self.background.blit(self.text1, self.rect1)
        
        self.window.blit(self.background, (0,0))
        pygame.display.flip()

        self.sprites = pygame.sprite.RenderUpdates()
        self.sprites_to_update=pygame.sprite.RenderUpdates()
        
        self.block_grid=[[Block(5,(0,0)) for y in range (0,GRID_SIZE_Y,1)] for x in range (0,GRID_SIZE_X,1)]#y is the y axis
        x_coordinate=500
        for i in range (0,GRID_SIZE_X,1):
            x_coordinate+=BLOCK_WIDTH
            y_coordinate=80
            for j in range(0,GRID_SIZE_Y,1):
                #print i,j
                self.block_grid[i][j].rect.topleft=x_coordinate,y_coordinate
                self.block_grid[i][j].indexes=i,j
                self.sprites.add(self.block_grid[i][j])
                y_coordinate+=BLOCK_WIDTH
        self.current_tetra=[]#will have 4 INDEXES of blocks stored in it
        
    def handleEvents(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            elif event.type==pygame.KEYDOWN and self.current_tetra:
                if event.key==pygame.K_RIGHT:
                    self.moveTetra("right")
                        
                elif event.key==pygame.K_LEFT:
                     self.moveTetra("left")

                elif event.key==pygame.K_SPACE:
                    self.moveTetra("straight down")
                elif event.key==pygame.K_DOWN:
                    self.rotateTetra()
                    self.rotateTetra()
                    self.rotateTetra()
                elif event.key==pygame.K_UP:
                    self.rotateTetra()
                elif event.key==pygame.K_ESCAPE:
                    return False
                pygame.time.set_timer(pygame.KEYDOWN, WAITING_TIME)
            elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_ESCAPE:
                    return False
        return True

    def removeBlocksForRotation(self):
        self.block_grid[self.current_tetra[0][0]][self.current_tetra[0][1]].transform(5)
        self.block_grid[self.current_tetra[2][0]][self.current_tetra[2][1]].transform(5)
        self.block_grid[self.current_tetra[3][0]][self.current_tetra[3][1]].transform(5)

    def transformBlocksForRotation(self,b_type):
        self.block_grid[self.current_tetra[0][0]][self.current_tetra[0][1]].transform(b_type)
        self.block_grid[self.current_tetra[2][0]][self.current_tetra[2][1]].transform(b_type)
        self.block_grid[self.current_tetra[3][0]][self.current_tetra[3][1]].transform(b_type)
        
    def areBoxesFilled(self,index1,index2,index3):
        "checks if boxes are filled AND if indexes are not negative!"
        for i in (index1,index2,index3):
            if i[0]> (GRID_SIZE_X - 1):
                return True
            if self.block_grid[i[0]][i[1]].block_type!=5 and i not in self.current_tetra:
                #DEBUG print self.current_tetra
                #DEBUG print i
                #DEBUG print "ITS FILLED!"
                return True
        for i in (index1,index2,index3):
            if i[0]<0 or i[1]<0:
                return True
        return False
    
    def rotateTetra(self):
        b_type=self.block_grid[self.current_tetra[0][0]][self.current_tetra[0][1]].block_type
        if b_type!=1:
            if b_type==2:
                if self.current_tetra[0][1]+1==self.current_tetra[1][1]:#takes form of |
                    if not self.areBoxesFilled([self.current_tetra[1][0]-1,self.current_tetra[1][1]],
                                          [self.current_tetra[1][0]+1,self.current_tetra[1][1]],
                                          [self.current_tetra[1][0]+2,self.current_tetra[1][1]]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0]-1,self.current_tetra[1][1]]
                        self.current_tetra[2]=[self.current_tetra[1][0]+1,self.current_tetra[1][1]]
                        self.current_tetra[3]=[self.current_tetra[1][0]+2,self.current_tetra[1][1]]

                        self.transformBlocksForRotation(b_type)

                else :
                    if not self.areBoxesFilled([self.current_tetra[1][0],self.current_tetra[1][1]-1],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]+1],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]+2]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0],self.current_tetra[1][1]-1]
                        self.current_tetra[2]=[self.current_tetra[1][0],self.current_tetra[1][1]+1]
                        self.current_tetra[3]=[self.current_tetra[1][0],self.current_tetra[1][1]+2]

                        self.transformBlocksForRotation(b_type)
            elif b_type==3:
                if self.current_tetra[0][0]+1==self.current_tetra[1][0]:#T
                    if not self.areBoxesFilled([self.current_tetra[1][0],self.current_tetra[1][1]+1],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]-1],
                                          [self.current_tetra[1][0]+1,self.current_tetra[1][1]]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0],self.current_tetra[1][1]+1]
                        self.current_tetra[2]=[self.current_tetra[1][0],self.current_tetra[1][1]-1]
                        self.current_tetra[3]=[self.current_tetra[1][0]+1,self.current_tetra[1][1]]

                        self.transformBlocksForRotation(b_type)
                    
                elif self.current_tetra[0][1]-1==self.current_tetra[1][1]:#|-
                    if not self.areBoxesFilled([self.current_tetra[1][0]+1,self.current_tetra[1][1]],
                                          [self.current_tetra[1][0]-1,self.current_tetra[1][1]],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]-1]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0]+1,self.current_tetra[1][1]]
                        self.current_tetra[2]=[self.current_tetra[1][0]-1,self.current_tetra[1][1]]
                        self.current_tetra[3]=[self.current_tetra[1][0],self.current_tetra[1][1]-1]

                        self.transformBlocksForRotation(b_type)

                elif self.current_tetra[0][0]-1==self.current_tetra[1][0]:#upside down T
                    if not self.areBoxesFilled([self.current_tetra[1][0],self.current_tetra[1][1]-1],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]+1],
                                          [self.current_tetra[1][0]-1,self.current_tetra[1][1]]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0],self.current_tetra[1][1]-1]
                        self.current_tetra[2]=[self.current_tetra[1][0],self.current_tetra[1][1]+1]
                        self.current_tetra[3]=[self.current_tetra[1][0]-1,self.current_tetra[1][1]]

                        self.transformBlocksForRotation(b_type)

                elif self.current_tetra[0][1]+1==self.current_tetra[1][1]:#-|
                    if not self.areBoxesFilled([self.current_tetra[1][0]-1,self.current_tetra[1][1]],
                                          [self.current_tetra[1][0]+1,self.current_tetra[1][1]],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]+1]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0]-1,self.current_tetra[1][1]]
                        self.current_tetra[2]=[self.current_tetra[1][0]+1,self.current_tetra[1][1]]
                        self.current_tetra[3]=[self.current_tetra[1][0],self.current_tetra[1][1]+1]

                        self.transformBlocksForRotation(b_type)
                    
            elif b_type==4:
                if self.current_tetra[2][1]-1==self.current_tetra[1][1]:#>^>
                    if not self.areBoxesFilled([self.current_tetra[1][0]+1,self.current_tetra[1][1]+1],
                                          [self.current_tetra[1][0]+1,self.current_tetra[1][1]],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]-1]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0]+1,self.current_tetra[1][1]+1]
                        self.current_tetra[2]=[self.current_tetra[1][0]+1,self.current_tetra[1][1]]
                        self.current_tetra[3]=[self.current_tetra[1][0],self.current_tetra[1][1]-1]

                        self.transformBlocksForRotation(b_type)
                    #DEBUG print "rotate"
                    
                elif self.current_tetra[2][0]-1==self.current_tetra[1][0]:#V>V
                    if not self.areBoxesFilled([self.current_tetra[1][0]-1,self.current_tetra[1][1]+1],
                                          [self.current_tetra[1][0],self.current_tetra[1][1]+1],
                                          [self.current_tetra[1][0]+1,self.current_tetra[1][1]]):
                        self.removeBlocksForRotation()

                        self.current_tetra[0]=[self.current_tetra[1][0]-1,self.current_tetra[1][1]+1]
                        self.current_tetra[2]=[self.current_tetra[1][0],self.current_tetra[1][1]+1]
                        self.current_tetra[3]=[self.current_tetra[1][0]+1,self.current_tetra[1][1]]
                    
                        self.transformBlocksForRotation(b_type)
    def displayGrid(self):
        for sprite in self.sprites_to_update:
            sprite.update()
            #print sprite.rect.topleft
        self.sprites.draw(self.background)
        self.window.blit(self.background,(0,0))
        pygame.display.flip()

    def createNewCurrentTetra(self):
        "returns True if possible, False if cannot, meaning a loss"
        current_type=random.randint(TETRA_CHOICES[0],TETRA_CHOICES[1])#1 is 2x2, 2 is 1x4, 3 is upside down T, 4 is an S
        if current_type==1:#square
            if (self.block_grid[4][0].block_type==5 and self.block_grid[5][0].block_type==5
                and self.block_grid[4][1].block_type==5 and self.block_grid[5][1].block_type==5):
                self.current_tetra.append([4,0])
                self.current_tetra.append([4,1])
                self.current_tetra.append([5,0])
                self.current_tetra.append([5,1])
            else:
                return False
                
        elif current_type==2:#line
            if (self.block_grid[4][0].block_type==5 and self.block_grid[4][2].block_type==5
                and self.block_grid[4][1].block_type==5 and self.block_grid[4][3].block_type==5):
                self.current_tetra.append([4,0])
                self.current_tetra.append([4,1])
                self.current_tetra.append([4,2])
                self.current_tetra.append([4,3])
            else:
                return False
            
        elif current_type==3:#T
            if (self.block_grid[4][0].block_type==5 and self.block_grid[5][0].block_type==5
                and self.block_grid[5][1].block_type==5 and self.block_grid[6][0].block_type==5):
                self.current_tetra.append([4,0])
                self.current_tetra.append([5,0])
                self.current_tetra.append([6,0])
                self.current_tetra.append([5,1])
            else:
                return False

        elif current_type==4:#S
            if (self.block_grid[5][0].block_type==5 and self.block_grid[6][0].block_type==5
                and self.block_grid[4][1].block_type==5 and self.block_grid[5][1].block_type==5):
                self.current_tetra.append([4,1])
                self.current_tetra.append([5,0])
                self.current_tetra.append([5,1])
                self.current_tetra.append([6,0])
            else:
                return False
        for index in self.current_tetra:
            self.block_grid[index[0]][index[1]].transform(current_type)
        return True
            
    def moveTetra(self,movement):
        temp_tetra=[]
        try:
            current_type=self.block_grid[self.current_tetra[0][0]][self.current_tetra[0][1]].block_type
        except IndexError:
            pass
        if movement=="down":        
            for index in self.current_tetra:
                if index[1]+1<=GRID_SIZE_Y-1:
                    if self.block_grid[index[0]][index[1]+1].block_type==5 or [index[0],index[1]+1] in self.current_tetra:
                        
                        temp_tetra.append([index[0],index[1]+1])                        
                    else:
                        self.current_tetra=[]
                        return False
                else:
                    self.current_tetra=[]
                    return False

        elif movement=="straight down":
            while True:
                if not self.moveTetra("down"):
                    #DEBUG print "YES"
                    #return False
                    break
        elif movement=="left":
            for index in self.current_tetra:
                if index[0]-1>=0:
                    if self.block_grid[index[0]-1][index[1]].block_type==5 or [index[0]-1,index[1]] in self.current_tetra:
                        
                        temp_tetra.append([index[0]-1,index[1]])
                    else:
                        return False
                else:
                    #DEBUG print "WHOOPS!"
                    return False
        elif movement=="right":
            for index in self.current_tetra:
                if index[0]+1<=GRID_SIZE_X-1:
                    if self.block_grid[index[0]+1][index[1]].block_type==5 or [index[0]+1,index[1]] in self.current_tetra:
                        
                        temp_tetra.append([index[0]+1,index[1]])
                            
                    else:
                        return False
                else:
                    #DEBUG print "WHOOPS!"
                    return False

        for index in self.current_tetra:
            self.block_grid[index[0]][index[1]].transform(5)
        for index in temp_tetra:
            self.block_grid[index[0]][index[1]].transform(current_type)
        self.current_tetra=temp_tetra+[]
        self.displayGrid()    
        return True

    def clearLines(self):
        is_line_full=False
        for y in range (GRID_SIZE_Y-1,-1,-1):
            if is_line_full:
                y+=1
            else:
                is_line_full=True
            for x in range(0,GRID_SIZE_X,1):
                if self.block_grid[x][y].block_type==5:
                    is_line_full=False
                    break
            if is_line_full:
                for z in range (y-1,-1,-1):
                    for x in range(0,GRID_SIZE_X,1):
                        self.block_grid[x][z+1].transform(self.block_grid[x][z].block_type)
                for x in range(0,GRID_SIZE_X,1):
                        self.block_grid[x][0].transform(5)
                        
                self.score+=1
                self.text1 = self.font1.render('Score: %s'%self.score, 1, BLACK)
                self.background.fill(GRAY,self.rect1)
                self.background.blit(self.text1, self.rect1)
                self.window.blit(self.background, (0,0))
        self.displayGrid()

    def reset(self):
        #is_line_full=True
        for y in range (GRID_SIZE_Y-1,-1,-1):
            for x in range(0,GRID_SIZE_X,1):
                    self.block_grid[x][y].transform(5)
            #if is_line_full:
            #y+=1
            #for z in range (y-1,-1,-1):
            #    for x in range(0,GRID_SIZE_X,1):
            #        print 'Y ' + str(y) + '-Z ' + str(z) + '-X ' + str(x)
            #        self.block_grid[x][z+1].transform(self.block_grid[x][z].block_type)
            #for x in range(0,GRID_SIZE_X,1):
            #        self.block_grid[x][0].transform(5)
    
            #self.score+=1
            
            self.window.blit(self.background, (0,0))
        #DEBUG print 'escaping'
        self.displayGrid()
                
    def run(self):        
        #DEBUG print 'Starting Event Loop'
        running = True
        lost=False
        runTime = 0
        rawCycleCount = 0
        hasAudioPlayed = 0
        while running and not lost:
            self.clock.tick(FPS)
            rawCycleCount = rawCycleCount + 1
            runTime = runTime + ((1/float(FPS)) * 1000)
            if (runTime > TIME_START_AUDIO) and (hasAudioPlayed == 0):
                hasAudioPlayed = 1
                AudioNarrative.play()
            if not self.current_tetra:
                if not self.createNewCurrentTetra():
                    #self.text1 = self.font1.render('Game Over. Score: %s'%self.score, 1, RED)
                    self.reset()
                    self.text1 = self.font1.render('Resetting', 1, RED)
                    self.background.fill(GRAY,self.rect1)
                    self.background.blit(self.text1, [616,150])
                    self.window.blit(self.background, (0,0))
                    pygame.display.flip()
                    pygame.time.wait(1500)
                    Scores.append(self.score)
                    self.score = 0
            else:
                running=self.handleEvents()
                
                self.moveTetra("down")
            if (runTime > GAME_LENGTH):
                lost=True
            if not self.current_tetra:
                self.rect1.top = 150
                self.rect1.left = 880
                self.text1 = self.font1.render('Score: %s'%self.score + '                             ', 1, BLACK)
                self.rect1 = self.text1.get_rect()
                self.rect1.top = 150
                self.rect1.left = 830
                self.background.fill(GRAY,self.rect1)
                self.background.blit(self.text1, self.rect1)
                self.window.blit(self.background, (0,0))

                self.clearLines()
                
                #self.displayGrid()
            
        #FROM OLD CODE - DON'T NEED ANYMORE
        #while lost:
        #    lost=self.handleEvents()

        #append the final score to the list
        Scores.append(self.score)

        #DEBUG print int(runTime)
        #DEBUG print rawCycleCount

    def run_practice(self):        
        #DEBUG print 'Starting Event Loop'
        running = True
        lost=False
        runTime = 0
        rawCycleCount = 0
        hasAudioPlayed = 0
        while running and not lost:
            self.clock.tick(FPS)
            rawCycleCount = rawCycleCount + 1
            runTime = runTime + ((1/float(FPS)) * 1000)
            if not self.current_tetra:
                if not self.createNewCurrentTetra():
                    #self.text1 = self.font1.render('Game Over. Score: %s'%self.score, 1, RED)
                    self.reset()
                    self.text1 = self.font1.render('Resetting', 1, RED)
                    self.background.blit(self.text1, [616,150])
                    self.window.blit(self.background, (0,0))
                    pygame.display.flip()
                    pygame.display.flip()
                    pygame.time.wait(1500)
                    Scores.append(self.score)
                    self.score = 0
            else:
                running=self.handleEvents()
                
                self.moveTetra("down")
            if (runTime > PRACTICE_LENGTH):
                lost=True
            if not self.current_tetra:
                self.rect1.top = 150
                self.rect1.left = 880
                self.text1 = self.font1.render('Score: %s'%self.score + '                             ', 1, BLACK)
                self.rect1 = self.text1.get_rect()
                self.rect1.top = 150
                self.rect1.left = 830
                self.background.fill(GRAY,self.rect1)
                self.background.blit(self.text1, self.rect1)
                self.window.blit(self.background, (0,0))
                self.clearLines()
            
        #FROM OLD CODE - DON'T NEED ANYMORE
        #while lost:
        #    lost=self.handleEvents()

    def readyscreen(self,textInput,waitTime):
        textDisp = basicFont.render(textInput, True, WHITE)
        textRect = textDisp.get_rect()
        textRect.left = 800 - (textDisp.get_width() / 2)
        textRect.top = 250
        self.background.fill(BLACK)
        self.background.blit(textDisp, textRect)
        self.window.blit(self.background, (0,0))
        pygame.display.flip()
        pygame.time.wait(waitTime)

        self.rect1.top = 150
        self.rect1.left = 880
        self.text1 = self.font1.render('Score: %s'%self.score + '                             ', 1, BLACK)
        self.rect1 = self.text1.get_rect()
        self.rect1.top = 150
        self.rect1.left = 830
        self.background.fill(GRAY)
        self.background.fill(GRAY,self.rect1)
        self.background.blit(self.text1, self.rect1)
        self.window.blit(self.background, (0,0))
        self.clearLines()

    def exitscreen(self,textInput,waitTime):
        textDisp = basicFont.render(textInput, 1, WHITE)
        textRect = textDisp.get_rect()
        textRect.left = 675 - (textDisp.get_width() / 2)
        textRect.top = 250
        self.background.fill(BLACK)
        self.background.blit(textDisp, textRect)
        self.window.blit(self.background, (0,0))
        pygame.display.flip()
        pygame.time.wait(waitTime)

        #quit pygame
        pygame.quit()
    
#run main function only if the module is executed as the main script
#(if you import this as a module then nothing is executed)

if __name__=="__main__":

    #Define game instance
    game = Game()
    #Show ready prompt, wait for user input
    game.readyscreen('READY',5000)
    #Run trial
    game.run()
    #show exit screen
    game.exitscreen('TIME UP',2500)

    #counters for writing scores and calculating # of times restarted
    i = 0
    resetCount = -1
    #open file for recording scores and resets
    dataFilename = open('uudtDataLog.csv', 'a')
    dataFile = csv.writer(dataFilename, delimiter=',', quotechar='|')

    #write scores to file, along with date and time of recording    
    dataFile.writerow(['New recording - ' + time.asctime(time.localtime(time.time()))])
    for i in range(len(Scores)):
        #DEBUG print 'Score: ' + str(Scores[i])
        resetCount += 1
        dataFile.writerow([Scores[i]])

    #Write number of times the game was reset
    dataFile.writerow(['Times restarted: ' + str(resetCount)])

    #Close the file
    dataFilename.close()
